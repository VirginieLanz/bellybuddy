import {
    IonTabs,
    IonTabBar,
    IonTabButton,
    IonRouterOutlet, IonFooter, IonToolbar,
} from "@ionic/react";
import React, {useEffect} from "react";
import {Redirect, Route} from "react-router";
import Homepage from "./Homepage";
import wcSession from "./wcSession";
import Advices from "./Advices";
import Archives from "./Archives";
import Analyzes from "./Analyzes";
import styled from "styled-components";
import ArchiveDetails from "./ArchiveDetails";

const TabRootPage: React.FC = () => {

    return (
        <Content>
            <IonRouterOutlet>
                <Redirect exact path="/tabs" to="/tabs/homepage"/>
                <Route
                    path="/tabs/homepage"
                    component={Homepage}
                    exact={true}
                />
                <Route
                    path="/tabs/archives"
                    component={Archives}
                    exact={true}
                />
                <Route
                    path="/wc-session"
                    component={wcSession}
                    exact={true}
                />
                <Route
                    path="/tabs/analyzes"
                    component={Analyzes}
                    exact={true}
                />
                <Route
                    path="/tabs/advices"
                    component={Advices}
                    exact={true}
                />
            </IonRouterOutlet>
            {/*-- Tab bar --*/}
                <IonTabBar slot="bottom" className="tab-bar">
                    <IonTabButton className="hover"  tab="homepage" href="/tabs/homepage">
                        <svg width="23" height="20" fill="none" xmlns="http://www.w3.org/2000/svg"><path className="primary" d="M9.2 19.55v-6.9h4.6v6.9h5.75v-9.2H23L11.5 0 0 10.35h3.45v9.2H9.2Z" fill="#263238"/></svg>
                        <p>Accueil</p>
                    </IonTabButton>
                    <IonTabButton className="hover"  tab="archives" href="/tabs/archives">
                        <svg width="17" height="20" fill="none" xmlns="http://www.w3.org/2000/svg"><path className="primary" d="M12.526 0H1.79C.805 0 0 .805 0 1.79v12.526h1.79V1.789h10.736V0Zm-.894 3.579L17 8.947v8.948c0 .984-.805 1.79-1.79 1.79H5.36c-.985 0-1.781-.806-1.781-1.79l.009-12.527c0-.984.796-1.79 1.78-1.79h6.264Zm-.895 6.263h4.92l-4.92-4.921v4.921Z" fill="#263238"/></svg>
                        <p>Historique</p>
                    </IonTabButton>
                    <IonTabButton tab="wcSession" href="/wc-session" className="hover button__add">
                        <div className="buttonWrapper">
                            <svg width="30" height="30" viewBox="0 0 30 30" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M30 17.1429H17.1429V30H12.8571V17.1429H0V12.8571H12.8571V0H17.1429V12.8571H30V17.1429Z" fill="white"/>
                            </svg>
                        </div>
                        <p className="margin">Session WC</p>
                    </IonTabButton>
                    <IonTabButton className="hover"  tab="analyzes" href="/tabs/analyzes">
                        <svg width="18" height="18" fill="none" xmlns="http://www.w3.org/2000/svg"><path className="primary" d="M16 0H2C.9 0 0 .9 0 2v14c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V2c0-1.1-.9-2-2-2ZM6 14H4V7h2v7Zm4 0H8V4h2v10Zm4 0h-2v-4h2v4Z" fill="#263238"/></svg>
                        <p>Analyses</p>
                    </IonTabButton>
                    <IonTabButton className="hover" tab="advices" href="/tabs/advices">
                        <svg width="22" height="21" fill="none" xmlns="http://www.w3.org/2000/svg"><path className="primary" d="m22 10.46-2.44-2.78L19.9 4l-3.61-.82L14.4 0 11 1.46 7.6 0 5.71 3.18l-3.61.81.34 3.68L0 10.46l2.44 2.78-.34 3.69 3.61.82 1.89 3.18 3.4-1.47 3.4 1.46 1.89-3.18 3.61-.82-.34-3.68L22 10.46Zm-10 5h-2v-2h2v2Zm0-4h-2v-6h2v6Z" fill="#263238"/></svg>
                        <p>Conseils</p>
                    </IonTabButton>
                </IonTabBar>
        </Content>
    );
};

const Content = styled(IonTabs)`
  ion-tabs { contain: none; }
  ion-tab-bar { contain: none; }

  // a wrapper for the position
  .buttonWrapper {
    position: absolute;
    bottom: 26px;
    height: 60px;
    width: 60px;
    background: ${props => props.theme.primary};
    border-radius: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
  }
  
  .button__add p {
    width: 100%;
  }

  
  .button__add {
    --padding-start: 0;
    --padding-end: 0;
    .margin {
      margin-top: 24px;
    }
  }

  // for the overflow
  ion-tab-button::part(native){
    overflow: visible;
  }
  
  .tab-bar {
    box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    border: 0;
    padding: 5px 0;
    position: fixed;
    bottom: 0;
    width: 100vw;

    p {
      font-size: ${props => props.theme.minus};
      line-height: initial;
      margin-top: 5px;
    }
  }
  .hover:hover {
    svg {
      .primary {
        fill: ${props => props.theme.primary};
      }
    }
  }
  .tab-selected {
    svg {
      .primary {
        fill: ${props => props.theme.primary};
      }
    }
  }
`
export default TabRootPage;