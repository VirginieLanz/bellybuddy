import {IonContent, IonPage} from '@ionic/react';
import React from "react";
import OpenHeader from "../components/OpenHeader";
import burgerMenu from "../assets/menu.svg";
import styled from "styled-components";
import Accordion from "../components/Accordion";
import AccordionDesc from "../components/AccordionDesc";


const Advices: React.FC = () => {

    return (
        <IonPage>
            <OpenHeader icon={burgerMenu}/>
            <Content>
                <div className="title">
                    <p><span className="title__primary">Améliorez</span> pas à pas</p>
                    <p><span className="title__primary">votre santé</span> intestinale</p>
                </div>
                <div className="resume__container">
                    <p className="subtitle">Conseils et solutions</p>
                    <div className="resume__content">
                        <Accordion title="Reconnaître et améliorer son type de selle" styledClass="primary">
                            <AccordionDesc title="Type 1 et 2 ― Constipation à constipation légère">
                                <p id="stool"><span className="bold">Type 1 : </span>Selles dures, séparées en morceaux, comme des
                                    noix (difficile à évacuer).</p>
                                <p><span className="bold">Type 2 : </span>En forme de saucisse, mais dure et grumeleuse.
                                </p>
                                <div className="tips__content margin">
                                    <p>Si le type 1 ou le type 2 sont des types de selles qui vous sont communs, voici
                                        ce que vous pouvez faire pour améliorer leur consistance : </p>
                                    <p>Il peut s’agir d’une constipation passagère liée à un changement transitoire
                                        (changement d’alimentation, position allongée prolongée, voyage…) qui ne dure
                                        jamais plus de quelques semaines, ou d’une constipation chronique si elle
                                        persiste depuis plus de 6 mois. On parle de constipation sévère si vous avez
                                        moins d’une selle par semaine.</p>
                                    <p>Les symptômes qui accompagnent habituellement la constipation sont les
                                        ballonnements, des flatulences et le mal de ventre. Les causes fréquentes
                                        incluent le manque de fibres dans l’alimentation, la déshydratation, le manque
                                        d’activité, la prise de certains médicaments, le stress, l’anxiété ou la
                                        dépression. Mais dans la plupart des situations, la constipation est temporaire
                                        et bénigne.</p>
                                    <p>Pour y remédier, le plus important est de <span className="bold">boire beaucoup d’eau </span>et
                                        de <span className="bold">manger suffisamment de fibres </span>comme des
                                        légumineuses et des fruits secs. L'<span
                                            className="bold">exercice physique </span>permet également de stimuler le
                                        transit intestinal.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Type 3 et 4 ― Normal">
                                <p><span className="bold">Type 3 : </span>Comme une saucisse, mais avec des craquelures
                                    à la surface.</p>
                                <p><span className="bold">Type 4 : </span>Ressemble à une saucisse, mais lisse à la
                                    surface.</p>
                                <div className="tips__content margin">
                                    <p>Des selles brunes, bien formées et faciles à expulser sont jugées saines.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Type 5, 6 et 7 ― Une tendance à la diarrhée à la diarrhée">
                                <p><span className="bold">Type 5 : </span>Selles solides mais molles, séparées les unes
                                    des autres (facile à évacuer).</p>
                                <p><span className="bold">Type 6 : </span>Morceaux déchiquetés, agglomérés en une
                                    matière pâteuse.</p>
                                <p><span className="bold">Type 7 : </span>Selles entièrement liquides.</p>
                                <div className="tips__content margin">
                                    <p>Si le type 5, 6 ou encore le type 7 sont des types de selles qui vous sont
                                        communs, voici ce que vous pouvez faire pour améliorer leur consistance : </p>
                                    <p>Les infections causées par des bactéries ou des virus sont la cause la plus
                                        fréquente de la diarrhée. Une intoxication alimentaire, certains médicaments,
                                        une hyperactivité de la thyroïde ou une maladie intestinale telle que la maladie
                                        de Crohn peuvent également être à l’origine de selles liquides.</p>
                                    <p>Certains aliments peuvent accélérer le transit, notamment l’alcool, la caféine et
                                        les aliments épicés ou gras. Le stress et l’anxiété peuvent aussi provoquer des
                                        diarrhées de façon occasionnelle.</p>
                                    <p>On distingue la diarrhée aiguë généralement causée par une infection virale, la
                                        diarrhée chronique causée par les maladies intestinales chroniques et les
                                        fausses diarrhées en réalité due à un épisode de constipation. En effet,
                                        l’intestin sécrète du liquide au contact des selles dures de la constipation, ce
                                        qui entraîne la production de selles molles associées aux selles dures.</p>
                                    <p>Le traitement des diarrhées dépend du facteur déclencheur, mais en règle
                                        générale, il est important de <span className="bold">s’hydrater </span>et
                                        d’<span className="bold">éviter les aliments que vous avez identifiés comme étant irritants pour votre système digestif</span>.
                                    </p>
                                    <p>Néanmoins, si les diarrhées persistent, il est recommandé de consulter un
                                        professionnel de santé car des selles molles, voire liquides, à répétition
                                        peuvent révéler un problème sous-jacent. Par exemple, le principal symptôme du
                                        syndrome de l’intestin irritable est la diarrhée. Ce syndrome touche 15% des
                                        personnes dans le monde. Il est aggravé en période de stress et il est fréquent
                                        chez les étudiants.</p>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Quels aliments privilégier quand on souffre de constipation ?" styledClass="forth">
                            <AccordionDesc title="Des fibres">
                                <div className="tips__content">
                                    <p>Les fibres alimentaires accélèrent le transit intestinal et aident ainsi à lutter contre la constipation. Le système digestif ne pouvant ni digérer, ni absorber entièrement les fibres alimentaires, afin de les évacuer, les intestins doivent fournir un effort particulier. Dans le tube digestif, les fibres alimentaires se gonflent d’eau, faisant apparaître plus vite la sensation de satiété et favorisant l’agglomération des selles. C’est à la fois cette capacité à retenir l’eau dans les selles et les contractions de la paroi intestinale provoquées par la présence de fibres qui facilitent le transit.</p>
                                    <p>Les fibres alimentaires sont présentes dans les graines, les céréales complètes, les légumineuses, les légumes et les fruits. Il en existe deux types : les fibres solubles et les fibres insolubles. Ce sont ces dernières qui fixent l’eau dans le tube digestif et favorise l’élimination des selles.</p>
                                    <p className="bold">Voici quelques exemples d’aliments sources de fibres :</p>
                                    <ul>
                                        <li>― Les céréales : blé entier, seigle, avoine, orge, sarrasin, boulgour, maïs, épeautre</li>
                                        <li>― Les légumes : carotte, courgette, courge, asperge, pomme de terre (à consommer de préférence sans la peau), patate douce, chou de Bruxelles, brocoli, pois, épinard, navet, haricot</li>
                                        <li>― Les fruits : orange, pamplemousse, pêche, nectarine, poire, pomme, papaye, fruits séchés (pruneaux, figues, dattes)</li>
                                    </ul>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Du pruneau">
                                <div className="tips__content">
                                    <p>Le pruneau est le fruit séché d’un type de prunier. Il est depuis longtemps reconnu pour ses bienfaits pour le transit : c’est un fruit à indice glycémique modéré mais très riche en fibres solubles et insolubles. Il favorise donc l’apparition de la sensation de satiété ainsi que le transit.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les produits céréaliers complets">
                                <div className="tips__content">
                                    <p className="bold">Une céréale est complète lorsque les trois éléments principaux qui la compose ont été conservés :</p>
                                    <ul>
                                        <li>― L'enveloppe (ou son), riche en fibres</li>
                                        <li>― Le germe, renfermant les nutriments</li>
                                        <li>― La graine, contenant glucides et protéines</li>
                                    </ul>
                                    <p>Une céréale est raffinée lorsque l’on supprime le son et le germe pour ne garder que le corps, ce qui l’appauvrit en fibres et nutriments tout en conservant son indice glycémique élevé.</p>
                                    <p>Les céréales complètes sont donc elles aussi source de fibres et participent au bon déroulement de notre transit intestinal.</p>
                                    <p>Voici quelques exemples des produits céréaliers complets : le pain, les biscottes ou les pâtes à base de farine complète, le riz complet, le maïs, la semoule complète, le boulgour, l'orge, l'avoine, le seigle, auxquels on peut associer certaines plantes considérées comme des “pseudo-céréales” telles que le sarrasin, le quinoa ou encore le lin.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Des prébiotiques et probiotiques">
                                <div className="tips__content">
                                    <p>Un déséquilibre des bactéries qui vivent naturellement dans vos intestins peut être à l'origine de problèmes digestifs. Les prébiotiques et probiotiques peuvent vous aider à renforcer votre système immunitaire et soutenir votre santé digestive globale. Les aliments probiotiques contiennent des bactéries vivantes. Les aliments prébiotiques nourrissent les bonnes bactéries qui vivent déjà dans votre intestin.</p>
                                    <p>Les probiotiques sont composés de bactéries bénéfiques qui aident votre corps à rester en bonne santé et à fonctionner correctement.</p>
                                    <p className="bold">Les aliments fermentés riches en probiotiques comprennent :</p>
                                    <ul>
                                        <li>― Le yaourt</li>
                                        <li>― Le tempe</li>
                                        <li>― Le fromage non-pasteurisé</li>
                                        <li>― Le kéfir</li>
                                        <li>― La choucroute</li>
                                        <li>― Les cornichons</li>
                                        <li>― Le kombucha</li>
                                        <li>― Le natto</li>
                                        <li>― Le kimchi</li>
                                        <li>― Le miso</li>
                                    </ul>
                                    <p className="bold">Les aliments riches en prébiotiques comprennent :</p>
                                    <ul>
                                        <li>― La banane</li>
                                        <li>― La pomme</li>
                                        <li>― L'orge</li>
                                        <li>― L'avoine</li>
                                        <li>― Les algues</li>
                                        <li>― Les asperges</li>
                                        <li>― L'ail</li>
                                        <li>― Les oignons</li>
                                        <li>― Les topinambours</li>
                                        <li>― Le miel</li>
                                    </ul>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Quels aliments éviter quand on souffre de constipation ?" styledClass="forth">
                            <AccordionDesc title="Les produits céréaliers raffinés">
                                <div className="tips__content">
                                    <p>Les produits céréaliers raffinés sont fabriqués à partir de farine dite “blanche”, par opposition à la farine complète. Ce type de farine ne contient que la graine de la céréale, elle est donc riche en glucides mais pauvre en nutriments et fibres. Consommés en grande quantité, les produits céréaliers à base de farine raffinée peuvent favoriser la prise de poids et ralentir le transit intestinal. Ils augmentent également le taux de sucre dans le sang (hyperglycémie), ce qui peut accroître le risque de diabète.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="La viande">
                                <div className="tips__content">
                                    <p>La viande, qu’elle soit rouge, blanche, grasse ou maigre, ne contient que très peu de fibres, et n’est donc pas recommandée pour lutter contre la constipation. La viande rouge est particulièrement à éviter puisqu’elle ralentit fortement le transit. C’est pour compenser cette absence de fibres qu’il est conseillé de l’accompagner de légumes si l’on souhaite en manger. Si vous souhaitez ou devez manger de la viande, par exemple pour son apport en protéines, préférez la viande maigre comme la volaille, mais veillez à ne pas consommer sa peau.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les matières grasses">
                                <div className="tips__content">
                                    <p>Indispensables au bon fonctionnement du corps, les matières grasses sont néanmoins à consommer avec modération et en évitant certains modes de cuisson si l’on peut préserver son transit. Le beurre et les huiles d’origine végétale (olive, colza, noix…) peuvent être utilisés dans la cuisine ou les assaisonnements : ils sont source de lipides et servent de lubrifiant naturel pour nos intestins. Les acides gras dits “trans”, “saturés” et “partiellement hydrogénées” (mentions présentes sur les emballages) sont en revanche à éviter lorsqu’on est constipé : on les retrouve par exemple dans les plats préparés, les pâtisseries ou la friture.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les aliments trop salés">
                                <div className="tips__content">
                                    <p>Tout comme les matières grasses, le sel est aussi nécessaire au corps humain et doit faire partie de nos apports nutritionnels quotidiens. Attention cependant à ne pas en abuser : une consommation excessive de sel peut causer la persistance dans l’organisme d’eau qui devrait en être évacuée, y compris dans les selles, c’est ce qu’on appelle la rétention d’eau. Pour donner plus de goût à vos préparations, vous pouvez remplacer le sel par certaines épices ou herbes aromatiques.</p>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Que faire en complément de ce régime anti-constipation ?" styledClass="forth">
                            <AccordionDesc title="Ne pas abuser des laxatifs">
                                <div className="tips__content">
                                    <p>Après un examen médical, un médecin peut vous prescrire un traitement adapté à vos besoins en fonction de votre constipation. En plus de règles hygiéno-diététiques à adopter, ce traitement peut comporter des laxatifs. Il est important cependant de ne pas en abuser, et d’éviter les laxatifs trop agressifs : on leur préfèrera des laxatifs dits “mucilagineux” qui gonflent et prennent une consistance visqueuse au contact de l’eau, agissant ainsi comme les fibres insolubles et favorisant l’agglomération et l’élimination des selles. Il existe de nombreux types de laxatifs ; certains pouvant être agressifs pour le système digestif, il est toujours préférable d'appliquer les conseils hygiéno-diététiques et de consulter un médecin si votre constipation ne disparaît pas d’elle-même.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Bien s’hydrater">
                                <div className="tips__content">
                                    <p>Bien s’hydrater participe au rétablissement du transit intestinal. Boire de l’eau est donc particulièrement conseillé en cas de constipation : l’eau complète l’effet des fibres sur la digestion et hydrate les selles, ce qui en facilite l’élimination. Lors d’épisodes de constipation, un professionnel de santé peut vous conseiller de boire une eau minérale riche en magnésium : cet oligoélément est connu pour son effet laxatif. Les jus de fruits frais peuvent également aider à améliorer le transit. Le jus de pomme, de raisin ou d’agrumes est particulièrement conseillé en cas de constipation. L’idéal est de ne pas ajouter de sucre à votre boisson. Comment puis-je prévenir la constipation ?</p>
                                    <p>La bonne nouvelle est que le plus souvent, de petits changements des habitudes peuvent réduire le risque de constipation :</p>
                                    <ul>
                                        <li>― Essayez de consommer 30 g de fibres par jour en mangeant plus de fruits, de légumes et de céréales complètes.</li>
                                        <li>― Préférez les abricots, les pommes, les prunes, les pruneaux et les raisins : ils contiennent naturellement du sorbitol, un sucre à effet laxatif doux.</li>
                                        <li>― Buvez beaucoup de liquides (environ 8-10 verres par jour), de préférence de l’eau, pour éviter la déshydratation.</li>
                                        <li>― Essayez d’aller aux toilettes à la même heure tous les matins et de prévoir suffisamment de temps pour aller à la selle.</li>
                                        <li>― Essayez de faire au moins 150 minutes d’activité physique par semaine, comme la marche, la course à pied, le vélo ou la natation.</li>
                                        <li>― Changez de position aux toilettes et essayez de poser vos pieds sur un petit marchepied. C’est un moyen simple de faciliter l’évacuation des selles.</li>
                                        <li>― Essayez des techniques de relaxation telles que les exercices de respiration profonde ou la méditation si vous sentez que des facteurs psychologiques sont à l’origine de votre constipation.</li>
                                    </ul>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Améliorer la couleur de ses selles" styledClass="primary">
                            <AccordionDesc title="Les selles noires">
                                <div className="tips__content">
                                    <p>La prise de comprimés de fer pour traiter une carence en fer est une cause fréquente d’assombrissement des selles, à l’instar de certains aliments tels que le réglisse. En effet, le tube digestif a tendance à mal absorber le fer.</p>
                                    <p>Mais les selles noires et charbonneuses peuvent également évoquer d’autres problèmes tels qu’un saignement digestif, un ulcère, la prise de certains médicaments, à des bactéries, etc. Il est préférable de consulter un médecin si vous remarquez cet aspect de vos selles car des examens complémentaires pourraient être nécessaires.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les selles rouges">
                                <div className="tips__content">
                                    <p>Les selles rouges peuvent attester de sang dans les selles mais elles peuvent également être colorées dû à l'ingestion d'aliments colorés. S'il s'agit de ce deuxième cas, nous avons une section concernant les selles colorées juste en dessous.</p>
                                    <p>La présence de sang rouge vif dans vos selles peut être alarmante mais elle est plus fréquente que l’on ne le pense. Elle peut être due à des fissures anales, des hémorroïdes, le syndrome de l’intestin irritable (SII) ou une diverticulite (un trouble digestif qui touche le côlon).</p>
                                    <p>Elle peut également évoquer un cancer de l’intestin. D’après les médecins, les signaux d’alerte d’un SII ou d’un cancer de l’intestin incluent :</p>
                                    <ul>
                                        <li>― Des douleurs abdominales persistantes</li>
                                        <li>― Des saignements de l’anus, en particulier si le sang est « mélangé » aux selles</li>
                                        <li>― Une perte de poids inexpliquée</li>
                                        <li>― Une modification récente de votre transit</li>
                                    </ul>
                                    <p>En cas de saignements dans les selles, il est préférable de réaliser des analyses médicales pour écarter des diagnostics alarmants. Parlez-en à votre médecin traitant afin de déterminer les examens à réaliser.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les selles colorées">
                                <div className="tips__content">
                                    <p>La couleur des selles est un très bon indicateur de la bonne santé de votre transit intestinal. La couleur des selles peut s’étendre du marron au violet. La plupart du temps, ce sont les aliments qui peuvent modifier la couleur de vos selles. Par exemple, la betterave peut donner une coloration rougeâtre, pouvant au premier coup d’œil être inquiétante. Une consommation importante de viande peut aussi foncer la couleur de vos selles. De même, une alimentation très riche en épinards et légumes verts peut colorer les selles en vert.</p>
                                    <p>Ce type de coloration est tout à fait bénin. Tant que les selles colorées restent passagères et que cela ne s’accompagnent pas d’autres symptômes, il n’y a pas besoin de s’inquiéter de ces changements.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Les selles pâles ou blanches/argileuses">
                                <div className="tips__content">
                                    <p>C’est la bile produite dans le foie et la vésicule biliaire qui donne sa coloration brune aux selles. Si vos selles sont claires, alors cela peut indiquer que le foie ou la vésicule biliaire ne produisent pas suffisamment de bile ou que son flux est obstrué. Si vos selles paraissent claires depuis plusieurs jours à plusieurs semaines sans amélioration, parlez-en à un médecin car des examens complémentaires pourraient être nécessaires.</p>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Améliorer l'odeur de ses selles" styledClass="primary">
                            <AccordionDesc title="Les selles très odorantes">
                                <div className="tips__content">
                                    <p>Si vos selles sentent plus mauvais que d’habitude, cela peut évoquer différents troubles, d’une constipation à une infection en passant par une intolérance alimentaire. Cela peut également être dû à un changement récent de votre alimentation. Si vous êtes inquiet ou que vous remarquez d’autres symptômes tels que des douleurs abdominales, de la fièvre ou du sang dans vos selles, parlez-en à votre médecin.</p>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                        <Accordion title="Comment améliorer son transit intestinal ?" styledClass="forth">
                            <p className="accordion__paragraph">La constipation est définie comme le fait d'aller à la selle moins de trois fois par semaine et lorsque vous y allez, vos selles sont dures, sèches et petites. La constipation peut être douloureuse, provoquant des crampes, une pression abdominale, des flatulences, un manque d'appétit et même des nausées.</p>
                            <AccordionDesc title="Quelles sont les principales causes d’un mauvais transit intestinal ?">
                                <div className="tips__content">
                                    <p>Un régime pauvre en fibres, une alimentation insuffisante, la déshydratation, les voyages, les changements de routine, le stress, le manque d'activité physique et les médicaments sont autant de facteurs qui peuvent impacter votre transit intestinal.</p>
                                </div>
                            </AccordionDesc>
                            <AccordionDesc title="Comment améliorer son transit intestinal ?">
                                <div className="tips__content">
                                    <p>Il existe heureusement des moyens simples d’améliorer votre transit intestinal au quotidien.</p>
                                    <p className="bold">Buvez beaucoup d'eau</p>
                                    <p>Il est essentiel de boire suffisamment d'eau pour assurer un bon transit. L'eau chaude aide à décomposer les aliments plus rapidement que l'eau froide, ce qui vous aide à aller aux toilettes plus facilement et à réduire la constipation.</p>
                                    <p className="bold">Buvez du café</p>
                                    <p>Disons que la caféine est une arme à double tranchant. C'est un diurétique qui vous fait uriner et peut provoquer une déshydratation, qui peut à son tour conduire à la constipation. Mais d'un autre côté, si vous êtes suffisament hydraté, les effets stimulants de la caféine peuvent stimuler votre système gastro-intestinal. Ce n'est pas la solution la plus infaillible de cette liste mais retenez que, sous forme de thé ou de café, la caféine a des propriétés qui peuvent faciliter le passage à la selle.</p>
                                    <p className="bold">Augmentez votre consommation de fibres</p>
                                    <p>Manger plus de fruits et de légumes améliore votre transit intestinal car les fibres alimentaires qu’ils contiennent augmentent l’arrivée d’eau dans le tube digestif et rendent les selles plus molles, stimulent directement l’intestin. Les selles sont ainsi plus faciles à évacuer.</p>
                                    <p>En pratique, il est recommandé de consommer 25 grammes pour les femmes et de 38 grammes pour les hommes de fibres alimentaires par jour. Si vous ne consommez pas assez de fibres dans votre alimentation, ajoutez en augmentant les quantités petit à petit des aliments riches en fibres à vos repas, notamment des fruits, des légumes, des haricots, du pain complet et des céréales. Manger plus de 70 grammes de fibres par jour peut provoquer l’effet inverse de celui recherché en entraînant des ballonnements et une constipation, alors n'en abusez pas.</p>
                                    <p>Grâce aux quantités élevées de fibres et de sorbitol contenues dans les pruneaux, ces derniers facilitent grandement le transit intestinal. Le sorbitol est un alcool de sucre que l'on trouve dans les fruits et qui peut accélérer le système gastro-intestinal et même provoquer des effets laxatifs s'il est consommé en excès. Outre les pruneaux, d'autres fruits fibreux peuvent également vous inciter. Les pommes et les poires sont faciles à transporter et à grignoter. Ces fruits sont remplis de fibres, grâce à la fois à leur chair et à leur peau. Ils ont également une teneur élevée en eau et contiennent du sorbitol et du fructose.</p>
                                    <p className="bold">Réduisez votre consommation d'alcool</p>
                                    <p>L'alcool est connu pour provoquer une déshydratation en raison de la perte de liquides due à une augmentation de la miction. Le fait de ne pas reconstituer les liquides perdus entraîne un risque accru de constipation. Il est donc conseillé de réduire sa consommation d'alcool.</p>
                                    <p className="bold">Évitez les aliments transformées</p>
                                    <p>Les viandes salées et transformées peuvent provoquer une constipation. Tout d'abord, elles ne contiennent pas de fibres et peuvent être consommées au détriment des fruits, des légumes, des légumineuses et des céréales complètes riches en nutriments et en fibres.</p>
                                    <p>Ces viandes sont également très salées or le sel peut absorber l'eau contenue dans les selles, ce qui les rend plus difficiles à évacuer.</p>
                                    <p>Et comme ils sont riches en graisses, l'organisme a besoin de plus d'énergie et de temps pour les décomposer. Il en va de même pour les aliments frits. Plus l'aliment est riche en graisses, plus le corps met du temps à le digérer. Manger trop de graisses peut donc ralentir la motilité gastro-intestinale et provoquer un certain blocage.</p>
                                    <p>Les céréales transformées peuvent également jouer un rôle dans la formation d’un bouchon intestinal, aussi appelé fécalome.</p>
                                    <p className="bold">Mangez un petit-déjeuner léger</p>
                                    <p>Prendre un petit déjeuner léger peut aider à stimuler le réflexe gastro colique en facilitant la digestion. Vous pouvez donc essayer d'aller aux toilettes 15 à 45 minutes après le petit déjeuner. A l’inverse, un petit déjeuner lourd pourrait ralentir votre digestion et provoquer la constipation.</p>
                                    <p className="bold">Instaurez une routine pour aller aux toilettes</p>
                                    <p>Certaines personnes vont aux toilettes à la première heure du matin, pour d'autres, à la fin de la journée, choisissez ce qui vous convient le mieux, mais aller à la selle à la même heure chaque jour et à un moment où vous ne vous sentez pas pressé peut vous aider à améliorer votre transit intestinal.</p>
                                    <p className="bold">Adoptez une bonne position aux toilettes</p>
                                    <p>Votre position aux toilettes peut également être à l'origine de la constipation. En général, une personne s'assoit sur la cuvette des toilettes occidentales comme elle le ferait sur une chaise : à 90 degrés avec les hanches pliées. Le placement de vos jambes à 90 degrés par rapport à votre abdomen perturbe le passage au niveau de vos intestins, ce qui rend l'évacuation des selles plus difficile. Pencher le haut de son corps vers l'avant lorsque l’on est assis ne facilite pas non plus l'évacuation des selles contrairement à certaines idées reçues : cela bloque encore plus.</p>
                                    <p>Le réflexe naturel de l'homme est de s'accroupir pour vider ses selles. Lorsque vous êtes en position accroupie, vos genoux sont rapprochés de votre abdomen, ce qui positionne vos organes et vos muscles de manière à placer votre rectum en ligne droite qui sera plus à même de se détendre pour faciliter l’exonération des selles.</p>
                                    <p>Bien sûr, vous ne pouvez pas vous accroupir sur votre cuvette de WC ordinaire mais vous pouvez néanmoins placer un tabouret bas devant votre siège de WC et poser vos pieds sur le tabouret. Cela vous aidera à obtenir un angle de 35 degrés au niveau des hanches, sans constriction du rectum, ce qui permettra aux selles de passer plus facilement.</p>
                                    <p className="bold">Faites régulièrement de l'exercice</p>
                                    <p>Un exercice régulier est essentiel pour votre bien-être général. La contraction musculaire est directement liée au flux sanguin, moins de flux sanguin signifie des contractions plus faibles, ce qui équivaut à un temps de transit des aliments plus lent. Lorsque vous faites de l'exercice, en particulier en pratiquant un entraînement cardio, vous augmentez le flux sanguin dans votre corps. Un flux sanguin plus fort traverse donc les muscles intestinaux, ce qui contribue à stimuler la contraction naturelle des muscles dans vos intestins et à diminuer le temps de transit des aliments</p>
                                    <p>Il n'est pas nécessaire de faire une séance d'entraînement longue ou vigoureuse, même une courte marche matinale de 15 à 30 minutes peut aider à garder l'appareil digestif en bonne santé. S'étirer et même pratiquer du yoga peut également soulager la constipation en réduisant le stress et en augmentant le flux sanguin vers l'appareil digestif.</p>
                                    <p className="bold">Massez-vous le ventre</p>
                                    <p>Il est possible de réaliser un auto-massage du côlon environ 20 minutes avant d'aller à la selle. Pour le réaliser, il suffit de suivre le chemin naturel du côlon. Commencez par vous placer sur le dos, et balayez :</p>
                                    <ul>
                                        <li>1. Depuis le bas de l'abdomen droit</li>
                                        <li>2. depuis le bas de l'abdomen droit</li>
                                        <li>3. en travers</li>
                                        <li>4. vers le bas</li>
                                        <li>5. vers l'intérieur</li>
                                    </ul>
                                    <p>Réalisez ce balayage cinq à sept fois.</p>
                                </div>
                            </AccordionDesc>
                        </Accordion>
                    </div>
                </div>
                <p className="text__footer">Sources : <a href="https://www.livi.fr/en-bonne-sante/constipation-que-manger/">livi</a></p>
            </Content>
        </IonPage>
    );
};


const Content = styled(IonContent)`
  width: 85%;
  display: flex;
  align-self: center;

  .text__footer {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.extraSmall};
    margin-bottom: 130px;
    a {
      font-family: ${props => props.theme.sofiaProLight};
      color: ${props => props.theme.darkGrey};
    }
  }
  .no-padding {
    padding: 0;
  }

  .title {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px 0 40px 0;

    .title__primary {
      color: ${props => props.theme.primary};
    }
  }

  .subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.small};
    margin-bottom: 10px;
  }

  .resume__content {
    display: flex;
    flex-direction: column;
    justify-content: center;
    gap: 20px;

    .bold {
      font-family: ${props => props.theme.sofiaProSemiBold};
    }

    &:last-child {
      margin-bottom: 30px;
    }
  }

  .tips__content {
    &.margin {
      margin-top: 20px;
    }
    p {
      margin-bottom: 10px;
    }
  }
`


export default Advices;
