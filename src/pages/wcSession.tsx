import {IonContent, IonPage, IonIcon} from '@ionic/react';
import {chevronForwardOutline} from 'ionicons/icons';

import React from "react";
import styled from "styled-components";
import {Button} from "../components/Button";
import play from "../assets/play.svg";
import { useHistory } from "react-router-dom";

const wcSession: React.FC= () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    let timeVariable = new Date();
    let mins = ('0'+timeVariable.getMinutes()).slice(-2);
    let currentTime =timeVariable.getHours() + 'h' + mins;

    // eslint-disable-next-line react-hooks/rules-of-hooks
    let history = useHistory();
    function handleClick() {
        history.goBack();
    }


    return (
        <Page className="wc-session">
            <Content fullscreen>
                    <Intro className="container">
                        <div className="intro__title">Vous êtes sur le point de démarrer une nouvelle session WC.</div>
                        <div className="intro__img">
                            <img src={play} alt=""/>
                        </div>
                        <div className="intro__time">
                            <p>Heure actuelle :</p>
                            <p>{currentTime}</p>
                        </div>
                        <Button
                            className="intro__cta"
                            as='link'
                            // 'to' is required since it's required in the Link component
                            to='/form'
                            styleType='button--border-white'
                            onClick={(evt) => {
                                // evt should be of type React.MouseEvent<HTMLAnchorElement, MouseEvent>
                                console.log(evt)
                            }}
                        >
                            Commencer
                            <IonIcon icon={chevronForwardOutline}></IonIcon>
                        </Button>
                        <button onClick={handleClick}>Annuler</button>
                    </Intro>
            </Content>
        </Page>
    );
};


const Content = styled(IonContent)`
  --overflow: hidden;
  .no-padding {
    padding: 0;
  }
  
  button {
    background: none;
    color: ${props => props.theme.white};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    font-family: ${props => props.theme.sofiaProLight};
  }
`

const Page = styled(IonPage)`
  --overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  --ion-background-color: ${props => props.theme.gradientPrimary};
`

const Intro = styled.div`

  display: flex;
  flex-direction: column;
  height: 100%;
  justify-content: center;
  color: ${props => props.theme.white};
  align-items: center;
  .intro__title {
    font-size: ${props => props.theme.large};
    font-family: ${props => props.theme.sofiaProSemiBold};
    margin-bottom: 20px;
  }
  
  .intro__img {
    img {
      height: 200px;
    }
  }
  
  .intro__time {
    display: flex;
    flex-direction: column;
    font-size: ${props => props.theme.small};
    font-family: ${props => props.theme.sofiaProLight};
    margin-bottom: 20px;
  }
  
`

export default wcSession;
