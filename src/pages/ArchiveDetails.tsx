import React, {useState, useEffect} from "react";
import styled from "styled-components";
import OpenHeader from "../components/OpenHeader";
import burgerMenu from "../assets/menu.svg";
import {
    IonContent, IonTabs
} from "@ionic/react";
import {useLocation} from 'react-router-dom';
import PainResultBlock from "../components/PainResultBlock";
import strongPain from "../assets/strong-pain.svg";
import SessionBadResultBlock from "../components/SessionBadResultBlock";
import SessionResultBlock from "../components/SessionResultBlock";
import littlePain from "../assets/little-pain.svg";
import moderatePain from "../assets/moderate-pain.svg";
import TabRootPage from "./TabRootPage";

interface ContainerProps {
    data: any;
    history: any;
    match: any;
}

//TODO: Afficher tabrootPage
const ArchiveDetails: React.FC<ContainerProps> = ({data, history, match}) => {
    const {path} = match;
    const {id} = match.params;
    const {state} = useLocation();
    const [filterArchives, setFilterArchives] = useState<string>('today');
    // State est un objet
    const [dataSession, setDataSession] = useState<any>({});
    const [symptomsArray, setSymptomsArray] = useState<any>([]);
    const [noSymptom, setNoSymptom] = useState<any>();

    useEffect(() => {
        setDataSession(state);
        let array = [];
        // @ts-ignore
        for(let symptom in state.symptoms) {
            // @ts-ignore
            array.push(state.symptoms[symptom]);
        }
        setSymptomsArray(array);
        setNoSymptom(array[0]);
    }, [state]);

    return (
        <>
            <OpenHeader icon={burgerMenu}/>
            <MenuContent className="pages">
                <div className="title">
                    <p>Historique</p>
                </div>
                <div className="container__radius">
                    <div className="filter__content container">
                        <p>Rapport de la session WC <br/>enregistrée le {('0'+new Date(dataSession.date).getDay()).slice(-2)}.{new Date(dataSession.date).getMonth()}.{new Date(dataSession.date).getFullYear()} à {dataSession.hour}</p>
                    </div>
                    <div className="menu__content">
                        <div className="resume__container container">
                            <div className="resume__content">
                                <Block className="resume__block small-width">
                                    {dataSession.pain === 'Pas ou peu de douleurs' &&
                                    <PainResultBlock styledClass="low-background" title="Douleurs"
                                                     painImage={littlePain}/>
                                    }
                                    {dataSession.pain === 'Modérément' &&
                                    <PainResultBlock styledClass="moderate-background" title="Douleurs"
                                                     painImage={moderatePain}/>
                                    }
                                    {dataSession.pain === 'Fortement' &&
                                    <PainResultBlock styledClass="strong-background" title="Douleurs"
                                                     painImage={strongPain}/>
                                    }
                                </Block>
                                <Block className="resume__block large-width">
                                    {dataSession.stool === 'Type 1' || dataSession.stool === 'Type 7' ?
                                        <SessionBadResultBlock title="Type le plus fréquent"
                                                               result={dataSession.stool}/> :
                                        dataSession.stool === 'Type 3' || dataSession.stool === 'Type 4' ?
                                            <SessionResultBlock styledClass="low-border" title="Type le plus fréquent"
                                                                result={dataSession.stool}/> :
                                            <SessionResultBlock styledClass="moderate-border"
                                                                title="Type le plus fréquent"
                                                                result={dataSession.stool}/>
                                    }
                                </Block>
                            </div>
                            <div className="resume__content">
                                <Block className="resume__block medium-width">
                                    {dataSession.color === 'Noir' || dataSession.color === 'Rougeâtre' || dataSession.color === 'Pâle ou blanc' ?
                                        <SessionBadResultBlock title="Couleur" result={dataSession.color}/> :
                                        <SessionResultBlock styledClass="low-border" title="Couleur"
                                                            result={dataSession.color}/>
                                    }
                                </Block>
                                <Block className="resume__block medium-width">
                                    <SessionResultBlock styledClass="" title="Volume" result={dataSession.size}/>
                                </Block>
                            </div>
                            <div className="resume__content">
                                <Block className="resume__block duration full-width">
                                    {noSymptom === 'Pas de symptôme' ?
                                        <SessionResultBlock styledClass="low-bord" title="Symptômes" result="Pas de symptômes"/> :
                                        <SessionBadResultBlock title="Symptôme(s)" result={symptomsArray.map((symptom: any) => (
                                            <p className="symptom">
                                                {symptom}
                                            </p>
                                        ))}/>
                                    }
                                </Block>
                            </div>
                            <div className="resume__content">
                                <Block className="resume__block duration medium-width">
                                    {dataSession.effort === 'Excessif' ?
                                        <SessionBadResultBlock title="Effort d'évacuation"
                                                               result={dataSession.effort}/> :
                                        dataSession.effort === 'Modéré' ?
                                            <SessionResultBlock styledClass="moderate-border"
                                                                title="Effort d'évacuation"
                                                                result={dataSession.effort}/> :
                                            <SessionResultBlock styledClass="low-border" title="Effort d'évacuation"
                                                                result={dataSession.effort}/>
                                    }
                                </Block>
                                <Block className="resume__block medium-width">
                                    {dataSession.duration === '< 3 minutes' ?
                                        <SessionResultBlock styledClass="low-border" title="Durée"
                                                            result={dataSession.duration}/> :
                                        dataSession.duration === '3 à 5 minutes' ?
                                            <SessionResultBlock styledClass="moderate-border" title="Durée"
                                                                result={dataSession.duration}/> :
                                            <SessionBadResultBlock title="Durée" result={dataSession.duration}/>
                                    }
                                </Block>
                            </div>
                            <div className="resume__content">
                                <Block className="resume__block duration small-width">
                                    {dataSession.smell === 'Sans odeur' || dataSession.smell === 'Douce' ?
                                        <SessionResultBlock styledClass="low-border" title="Odeur"
                                                            result={dataSession.smell}/> :
                                        dataSession.smell === 'Modérée' ?
                                            <SessionResultBlock styledClass="moderate-border" title="Odeur"
                                                                result={dataSession.smell}/> :
                                            <SessionBadResultBlock title="Odeur" result={dataSession.smell}/>
                                    }
                                </Block>
                                <Block className="resume__block large-width">
                                    {dataSession.flatulence === 'Pas ou peu' ?
                                        <SessionResultBlock styledClass="low-border" title="Présence de flatulences"
                                                            result={dataSession.flatulence}/> :
                                        dataSession.flatulence === 'Modérée' ?
                                            <SessionResultBlock styledClass="moderate-border"
                                                                title="Présence de flatulences"
                                                                result={dataSession.flatulence}/> :
                                            <SessionBadResultBlock title="Présence de flatulences"
                                                                   result={dataSession.flatulence}/>
                                    }
                                </Block>
                            </div>
                            {dataSession.notes ?
                                <div className="resume__content notes">
                                    <Block className="resume__block duration full-width">
                                        <SessionResultBlock styledClass="notes" title="Notes"
                                                            result={dataSession.notes}/>
                                    </Block>
                                </div> :
                                <div className="resume__content">
                                    <Block className="resume__block duration full-width">
                                        <SessionResultBlock styledClass="" title="Notes" result="Pas de notes"/>
                                    </Block>
                                </div>
                            }
                        </div>
                    </div>
                    <TabRootPage/>
                </div>
            </MenuContent>
        </>
    );
};

const MenuContent = styled(IonContent)`
  .pages {
    position: relative;
    z-index: 999;
  }
  .no-padding {
    padding: 0;
  }

  .title {
    width: 85%;
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }

  .container__radius {
    background: ${props => props.theme.gradientPrimary};
    border-radius: 20px 20px 0 0;
    height: 60%;
    padding: 10px 0;
  }

  .filter__content {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.medium};
    padding: 10px 0;

    .filter {
      background: ${props => props.theme.white};
      padding: 7px 20px;
      border-radius: 50px;
      margin: 10px auto;
      
      ion-select::part(icon) {
        width: 20px;
      }
    }

    p {
      color: ${props => props.theme.white};
      margin-bottom: 10px;
    }
  }

  .menu__content {
    background: ${props => props.theme.white};
    border-radius: 20px 20px 0 0;
    padding: 20px 0;
    display: flex;
    flex-direction: column;
    gap: 20px;
    margin-bottom: 160px;
  }

  .resume__container {
    display: flex;
    flex-direction: column;
    gap: 20px;
  }

  .resume__data {
    .symptom {
      color: ${props => props.theme.white};
      font-family: ${props => props.theme.sofiaProLight};
      font-size: ${props => props.theme.small};
      background: ${props => props.theme.gradientSecondary};
      padding: 4px 15px 3px 15px;
      border-radius: 20px;
    }
  }
  .resume__content {
    display: flex;
    height: auto;
    gap: 20px;
    
    &.notes {
      .resume__block {
        min-height: min-content;
        height: min-content;
        padding-bottom: 10px;
      }
    }

    &.last-session {
      height: 110px;
    }

    .full-width {
      width: 100%;
    }

    .large-width {
      width: 70%;
    }
    
    .medium-width {
      width: 50%;
    }

    .small-width {
      width: 30%;
    }
  }
`

const Block = styled("div")`
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  text-align: center;
  background: ${props => props.theme.lightGrey};

  .primary-background {
    background: ${props => props.theme.gradientPrimary};
  }
`

const Content = styled(IonTabs)`
  ion-tabs { contain: none; }
  ion-tab-bar { contain: none }

  // a wrapper for the position
  .buttonWrapper {
    position: absolute;
    bottom: 26px;
    height: 60px;
    width: 60px;
    background: ${props => props.theme.primary};
    border-radius: 50px;
    display: flex;
    justify-content: center;
    align-items: center;
    box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
  }
  
  .button__add p {
    width: 100%;
  }

  
  .button__add {
    --padding-start: 0;
    --padding-end: 0;
    .margin {
      margin-top: 24px;
    }
  }

  // for the overflow
  ion-tab-button::part(native){
    overflow: visible;
  }
  
  .tab-bar {
    box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -webkit-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    -moz-box-shadow: 0px -5px 20px 0px rgba(0, 0, 0, 0.1);
    border: 0;
    padding: 5px 0;

    p {
      font-size: ${props => props.theme.minus};
      line-height: initial;
      margin-top: 5px;
    }
  }
  .hover:hover {
    svg {
      .primary {
        fill: ${props => props.theme.primary};
      }
    }
  }
  .tab-selected {
    svg {
      .primary {
        fill: ${props => props.theme.primary};
      }
    }
  }
`

export default ArchiveDetails;
