import {IonContent, IonPage, IonSelect, IonSelectOption} from '@ionic/react';
import React, {useState} from "react";
import OpenHeader from "../components/OpenHeader";
import burgerMenu from "../assets/menu.svg";
import styled from "styled-components";
import StoolTypeDataChart from "../data/StoolTypeDataChart";
import ColorDataChart from "../data/ColorDataChart";
import SmellDataChart from "../data/SmellDataChart";
import SizeDataChart from "../data/SizeDataChart";
import PainDataChart from "../data/PainDataChart";
import PremiumModal from "../components/PremiumModal";
import useModal from "../hooks/useModal";

const Analyzes: React.FC = () => {
    const [filterArchives, setFilterArchives] = useState<string>('stoolType');
    const { isShowing, toggle } = useModal();
    return (
        <IonPage>
            <OpenHeader icon={burgerMenu}/>
            <ContentPage>
                <div className="title container">
                    <p>Analyses</p>
                </div>
                <div className="container__radius">
                    <PremiumModal isShowing={isShowing} hide={toggle} title="Option premium"/>
                    <div className="filter container">
                        <IonSelect value={filterArchives} okText="Valider" cancelText="Annuler"
                                   onIonChange={e => setFilterArchives(e.detail.value)}>
                            <IonSelectOption value="stoolType">Type de selles</IonSelectOption>
                            <IonSelectOption value="color">Couleur</IonSelectOption>
                            <IonSelectOption value="smell">Odeur</IonSelectOption>
                            <IonSelectOption value="size">Volume</IonSelectOption>
                            <IonSelectOption value="pain">Douleur</IonSelectOption>
                            <IonSelectOption value="efforts">Efforts</IonSelectOption>
                            <IonSelectOption value="flatulences">Flatulences</IonSelectOption>
                            <IonSelectOption value="symptoms">Symptômes</IonSelectOption>
                            <IonSelectOption value="duration">Durée</IonSelectOption>
                        </IonSelect>
                    </div>
                    <div className="filter-period container">
                        <p className="selected modal-toggle">Semaine</p>
                        <p className="modal-toggle" onClick={(evt) => toggle()}>Mois</p>
                        <p className="modal-toggle" onClick={(evt) => toggle()}>Année</p>
                    </div>
                    {filterArchives === 'stoolType' &&
                        <div className="archives__content">
                            <StoolTypeDataChart/>
                        </div>
                    }
                    {filterArchives === 'color' &&
                    <div className="archives__content">
                        <ColorDataChart/>
                    </div>
                    }
                    {filterArchives === 'smell' &&
                    <div className="archives__content">
                        <SmellDataChart/>
                    </div>
                    }
                    {filterArchives === 'size' &&
                    <div className="archives__content">
                        <SizeDataChart/>
                    </div>
                    }
                    {filterArchives === 'pain' &&
                    <div className="archives__content">
                        <PainDataChart/>
                    </div>
                    }
                </div>
            </ContentPage>
        </IonPage>
    );
};

const ContentPage = styled(IonContent)`
  margin: 0 auto;

  .title {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }

  .subtitle {
    font-family: ${props => props.theme.sofiaProRegular};
    font-size: ${props => props.theme.small};
    margin: 0 auto 10px auto;
  }

  .filter {
    background: ${props => props.theme.white};
    padding: 7px 20px;
    border-radius: 50px;
    width: 80%;
    margin: 10px auto 20px auto;
  }
  
  .filter-period {
    display: flex;
    gap: 40px;
    color: ${props => props.theme.white};
    font-family: ${props => props.theme.sofiaProRegular};
    font-size: ${props => props.theme.small};
    margin-bottom: 20px;
    justify-content: center;
    p { cursor: pointer; }
    
    .selected {
      position: relative;
      &::after {
        content: "";
        width: calc(100% + 10px);
        height: 0;
        border-bottom: 1px solid ${props => props.theme.white};
        position: absolute;
        bottom: -3px;
        left: -5px;
      }
    }
  }

  .container__radius {
    background: ${props => props.theme.gradientPrimary};
    border-radius: 20px 20px 0 0;
    height: 60%;
    padding: 10px 0;
  }

  .archives__content {
    background: ${props => props.theme.white};
    border-radius: 20px 20px 0 0;
    height: 100%;
    padding: 5px 0;
  }

  .flex-content {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 25px;
  }
  
  .archives__content {
    canvas {
      margin-bottom: 100px;
    }
  }
`


export default Analyzes;
