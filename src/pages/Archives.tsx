import {IonContent, IonIcon, IonPage, IonSelect, IonSelectOption} from '@ionic/react';
import React, {useEffect, useState} from "react";
import OpenHeader from "../components/OpenHeader";
import burgerMenu from "../assets/menu.svg";
import {Storage} from "@capacitor/storage";
import strongPain from "../assets/strong-pain.svg";
import moderatePain from "../assets/moderate-pain.svg";
import littlePain from "../assets/little-pain.svg";
import {arrowForwardOutline} from "ionicons/icons";
import styled from "styled-components";
import FirstSessionWC from "../components/FirstSessionWC";
import NoSessionWC from "../components/NoSessionWC";
import { useHistory } from "react-router-dom";

//TODO: Filtrer les résultats et afficher soit le jour de la session soit la date entière (filtre TOUS)
//TODO: Ajouter un background à la modale
const Archives: React.FC = () => {
    // Tableau des sessions WC
    let sessionArray: any[] = [];
    // Tableau des sessions WC enregistrées durant les 7 derniers jours
    let sessionArray7: any[] = [];
    // Tableau des sessions WC enregistrées durant la journée d'hier
    let sessionArrayYesterday: any[] = [];
    // Tableau des sessions WC enregistrées durant la journée courante
    let sessionArrayToday: any[] = [];
    // Tableau des sessions WC enregistrées durant le dernier mois
    let sessionArrayLastMonth: any[] = [];
    let history = useHistory();

    const [sessionArrayTodayState, setSessionArrayTodayState] = useState<any>([]);
    const [sessionArrayYesterdayState, setSessionArrayYesterdayState] = useState<any>([]);
    const [sessionArrayLastMonthState, setSessionArrayLastMonthState] = useState<any>([]);
    const [sessionArray7State, setSessionArray7State] = useState<any>([]);
    const [sessionArrayState, setSessionArrayState] = useState<any>([]);
    const [displayArchiveDetails, setDisplayArchiveDetails] = useState<boolean>();
    const [filterArchives, setFilterArchives] = useState<string>('today');

    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});

        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
            setSessionArrayState(sessionArray);
        }
    };

    /**
     * Met à jour le tableau sessionArray7
     */
    const getSessionForTheLast7Days = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let sevenDaysAgoDate = currentDate.getDate() - 7; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de sevenDaysAgoDate
            if (new Date(sessionArray[i].date).getMonth() >= sevenDaysAgoDate) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArray7.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArray7State(sessionArray7);
    }

    /**
     * Met à jour le tableau sessionArrayLastMonth
     */
    const getSessionArrayLastMonth = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let oneMonthAgo = currentDate.getMonth() - 1; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande à la valeur de oneMonthAgo
            if (new Date(sessionArray[i].date).getMonth() <= oneMonthAgo) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArrayLastMonth.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArrayLastMonthState(sessionArrayLastMonth);
    }

    /**
     * Met à jour le tableau sessionArrayYesterday
     */
    const getSessionFromYesterday = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let yesterday = currentDate.getDate() - 1; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de yesterday
            if (new Date(sessionArray[i].date).getDate() === yesterday) {
                // Si oui, on a ajoute la session dans le tableau sessionArrayYesterday
                sessionArrayYesterday.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArrayYesterdayState(sessionArrayYesterday);
        console.log(sessionArrayYesterday)
    }

    /**
     * Met à jour le tableau sessionArrayToday
     */
    const getSessionFromToday = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let today = currentDate.getDate(); // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de yesterday
            if (new Date(sessionArray[i].date).getDate() === today) {
                // Si oui, on a ajoute la session dans le tableau sessionArrayYesterday
                sessionArrayToday.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArrayTodayState(sessionArrayToday);
    }

    const handleClick = () => {
        setDisplayArchiveDetails(true);
    }

    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            getSessionForTheLast7Days();
            getSessionFromYesterday();
            getSessionFromToday();
            getSessionArrayLastMonth();
        })
    }, []);

    return (
        <IonPage>
            <OpenHeader icon={burgerMenu}/>
            <ContentPage>
                <div className="title container">
                    <p>Historique</p>
                </div>
                <div className="container__radius">
                    <div className="filter container">
                        <IonSelect value={filterArchives} okText="Valider" cancelText="Annuler"
                                   onIonChange={e => setFilterArchives(e.detail.value)}>
                            <IonSelectOption value="today">Aujourd'hui</IonSelectOption>
                            <IonSelectOption value="yesterday">Hier</IonSelectOption>
                            <IonSelectOption value="last7days">7 derniers jours</IonSelectOption>
                            <IonSelectOption value="lastmonth">Le mois passé</IonSelectOption>
                            <IonSelectOption value="all">Tous</IonSelectOption>
                        </IonSelect>
                    </div>
                    {filterArchives === 'last7days' &&
                    <div className="archives__content">
                        <div className="subtitle container">
                            <p>7 derniers jours</p>
                        </div>
                        <div className="flex-content container">
                            {sessionArray7State.length === 0 ?
                                <NoSessionWC/>
                                :
                                <>
                                    {sessionArray7State.map((session: string | number, index: any) => (
                                        <Content
                                            className={`resume__session ${sessionArray7State[index].pain === 'Modérément' ? "moderate-pain" : sessionArray7State[index].pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
                                            <div className="session__data" key={index}>
                                                <div className="data--top">
                                                    <div className="data--top--left">
                                                        <p>{sessionArray7State[index].hour}</p>
                                                        <p>Durée : <span>{sessionArray7State[index].duration}</span></p>
                                                        <p>Type : <span>{sessionArray7State[index].stool}</span></p>
                                                    </div>
                                                    <div className="data--top--right">
                                                        {sessionArray7State[index].pain === 'Fortement' &&
                                                        <img src={strongPain} alt=""/>
                                                        }
                                                        {sessionArray7State[index].pain === 'Modérément' &&
                                                        <img src={moderatePain} alt=""/>
                                                        }
                                                        {sessionArray7State[index].pain === 'Pas ou peu de douleurs' &&
                                                        <img src={littlePain} alt=""/>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="data--bottom">
                                                    <p onClick={() => history.push({
                                                        pathname: '/tabs/archives/details',
                                                        state: sessionArray7State[index]
                                                    })}>
                                                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                                                </div>
                                            </div>
                                        </Content>
                                    ))}
                                </>
                            }
                        </div>
                    </div>
                    }
                    {filterArchives === 'all' &&
                    <div className="archives__content">
                        <div className="subtitle container">
                            <p>Tous</p>
                        </div>
                        <div className="flex-content container">
                            {sessionArrayState.length === 0 ?
                                <NoSessionWC/>
                                :
                                <>
                                    {sessionArrayState.map((session: string | number, index: any) => (
                                        <Content
                                            className={`resume__session ${sessionArrayState[index].pain === 'Modérément' ? "moderate-pain" : sessionArrayState[index].pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
                                            <div className="session__data" key={index}>
                                                <div className="data--top">
                                                    <div className="data--top--left">
                                                        <p>{('0'+new Date(sessionArrayState[index].date).getDay()).slice(-2)}.{new Date(sessionArrayState[index].date).getMonth()}.{new Date(sessionArrayState[index].date).getFullYear()} à {sessionArray7State[index].hour}</p>
                                                        <p>Durée : <span>{sessionArrayState[index].duration}</span></p>
                                                        <p>Type : <span>{sessionArrayState[index].stool}</span></p>
                                                    </div>
                                                    <div className="data--top--right">
                                                        {sessionArrayState[index].pain === 'Fortement' &&
                                                        <img src={strongPain} alt=""/>
                                                        }
                                                        {sessionArrayState[index].pain === 'Modérément' &&
                                                        <img src={moderatePain} alt=""/>
                                                        }
                                                        {sessionArrayState[index].pain === 'Pas ou peu de douleurs' &&
                                                        <img src={littlePain} alt=""/>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="data--bottom">
                                                    <p onClick={() => history.push({
                                                        pathname: '/tabs/archives/details',
                                                        state: sessionArrayState[index]
                                                    })}>
                                                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                                                </div>
                                            </div>
                                        </Content>
                                    ))}
                                </>
                            }
                        </div>
                    </div>
                    }
                    {filterArchives === 'yesterday' &&
                    <div className="archives__content">
                        <div className="subtitle container">
                            <p>Hier</p>
                        </div>
                        <div className="flex-content container">
                            {sessionArrayYesterdayState.length === 0 ?
                                <NoSessionWC/>
                                :
                                <>
                                    {sessionArrayYesterdayState.map((session: string | number, index: any) => (
                                        <Content
                                            className={`resume__session ${sessionArrayYesterdayState[index].pain === 'Modérément' ? "moderate-pain" : sessionArrayYesterdayState[index].pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
                                            <div className="session__data" key={index}>
                                                <div className="data--top">
                                                    <div className="data--top--left">
                                                        <p>{sessionArray7State[index].hour}</p>
                                                        <p>Durée
                                                            : <span>{sessionArrayYesterdayState[index].duration}</span>
                                                        </p>
                                                        <p>Type
                                                            : <span>{sessionArrayYesterdayState[index].stool}</span>
                                                        </p>
                                                    </div>
                                                    <div className="data--top--right">
                                                        {sessionArrayYesterdayState[index].pain === 'Fortement' &&
                                                        <img src={strongPain} alt=""/>
                                                        }
                                                        {sessionArrayYesterdayState[index].pain === 'Modérément' &&
                                                        <img src={moderatePain} alt=""/>
                                                        }
                                                        {sessionArrayYesterdayState[index].pain === 'Pas ou peu de douleurs' &&
                                                        <img src={littlePain} alt=""/>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="data--bottom">
                                                    <p onClick={() => history.push({
                                                        pathname: '/tabs/archives/details',
                                                        state: sessionArrayYesterdayState[index]
                                                    })}>
                                                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                                                </div>
                                            </div>
                                        </Content>
                                    ))}
                                </>
                            }
                        </div>
                    </div>
                    }
                    {filterArchives === 'today' &&
                    <div className="archives__content">
                        <div className="subtitle container">
                            <p>Aujourd'hui</p>
                        </div>
                        <div className="flex-content container">
                            {sessionArrayTodayState.length === 0 ?
                                <>
                                    <NoSessionWC/>
                                    <FirstSessionWC/>
                                </>
                                :
                                <>
                                    {sessionArrayTodayState.map((session: string | number, index: any) => (
                                        <Content
                                            className={`resume__session ${sessionArrayTodayState[index].pain === 'Modérément' ? "moderate-pain" : sessionArrayTodayState[index].pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
                                            <div className="session__data" key={index}>
                                                <div className="data--top">
                                                    <div className="data--top--left">
                                                        <p>{sessionArray7State[index].hour}</p>
                                                        <p>Durée
                                                            : <span>{sessionArrayTodayState[index].duration}</span>
                                                        </p>
                                                        <p>Type
                                                            : <span>{sessionArrayTodayState[index].stool}</span>
                                                        </p>
                                                    </div>
                                                    <div className="data--top--right">
                                                        {sessionArrayTodayState[index].pain === 'Fortement' &&
                                                        <img src={strongPain} alt=""/>
                                                        }
                                                        {sessionArrayTodayState[index].pain === 'Modérément' &&
                                                        <img src={moderatePain} alt=""/>
                                                        }
                                                        {sessionArrayTodayState[index].pain === 'Pas ou peu de douleurs' &&
                                                        <img src={littlePain} alt=""/>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="data--bottom">
                                                    <p onClick={() => history.push({
                                                        pathname: '/tabs/archives/details',
                                                        state: sessionArrayTodayState[index]
                                                    })}>
                                                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                                                </div>
                                            </div>
                                        </Content>
                                    ))}
                                </>
                            }
                        </div>
                    </div>
                    }
                    {filterArchives === 'lastmonth' &&
                    <div className="archives__content">
                        <div className="subtitle container">
                            <p>Le mois passé</p>
                        </div>
                        <div className="flex-content container">
                            {sessionArrayLastMonthState.length === 0 ?
                                <NoSessionWC/>
                                :
                                <>
                                    {sessionArrayLastMonthState.map((session: string | number, index: any) => (
                                        <Content
                                            className={`resume__session ${sessionArrayLastMonthState[index].pain === 'Modérément' ? "moderate-pain" : sessionArrayLastMonthState[index].pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
                                            <div className="session__data" key={index}>
                                                <div className="data--top">
                                                    <div className="data--top--left">
                                                        <p>{sessionArray7State[index].hour}</p>
                                                        <p>Durée
                                                            : <span>{sessionArrayLastMonthState[index].duration}</span>
                                                        </p>
                                                        <p>Type
                                                            : <span>{sessionArrayLastMonthState[index].stool}</span>
                                                        </p>
                                                    </div>
                                                    <div className="data--top--right">
                                                        {sessionArrayLastMonthState[index].pain === 'Fortement' &&
                                                        <img src={strongPain} alt=""/>
                                                        }
                                                        {sessionArrayLastMonthState[index].pain === 'Modérément' &&
                                                        <img src={moderatePain} alt=""/>
                                                        }
                                                        {sessionArrayLastMonthState[index].pain === 'Pas ou peu de douleurs' &&
                                                        <img src={littlePain} alt=""/>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="data--bottom">
                                                    <p onClick={() => history.push({
                                                        pathname: '/tabs/archives/details',
                                                        state: sessionArrayLastMonthState[index]
                                                    })}>
                                                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                                                </div>
                                            </div>
                                        </Content>
                                    ))}
                                </>
                            }
                        </div>
                    </div>
                    }
                </div>
            </ContentPage>
        </IonPage>
    );
};

const ContentPage = styled(IonContent)`
  margin: 0 auto;
  
  .archives__content:last-child {
    margin-bottom: 200px;
  }

  .title {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }

  .subtitle {
    font-family: ${props => props.theme.sofiaProRegular};
    font-size: ${props => props.theme.small};
    margin: 0 auto 10px auto;
  }

  .filter {
    background: ${props => props.theme.white};
    padding: 7px 20px;
    border-radius: 50px;
    width: 80%;
    margin: 10px auto 20px auto;

    ion-select::part(icon) {
      width: 20px;
    }
    
  }

  .container__radius {
    background: ${props => props.theme.gradientPrimary};
    border-radius: 20px 20px 0 0;
    height: 60%;
    padding: 10px 0;
  }

  .archives__content {
    background: ${props => props.theme.white};
    border-radius: 20px 20px 0 0;
    height: 100%;
    padding: 20px 0;
  }

  .flex-content {
    display: flex;
    flex-direction: column;
    align-items: center;
    gap: 25px;

    .resume__session:last-child {
      margin-bottom: 70px;
    }
  }
`


const Content = styled("div")`
  display: flex;
  gap: 5px;
  align-items: center;
  width: 90%;
  border-radius: 20px;
  padding: 12px 16px;
  font-family: ${props => props.theme.sofiaProMedium};
  font-size: ${props => props.theme.small};
  color: ${props => props.theme.white};

  &.moderate-pain {
    background: ${props => props.theme.gradientThird};
  }

  &.strong-pain {
    background: ${props => props.theme.gradientSecondary};
  }

  &.low-pain {
    background: ${props => props.theme.gradientForth};
  }

  .session__data {
    width: 100%;
    height: 100%;
    line-height: 130%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .data--top {
      display: flex;
      width: 100%;
      justify-content: space-between;

      .data--top--right {
        padding-top: 5px;
      }
    }

    .data--bottom {
      display: flex;
      justify-content: flex-end;

      p {
        cursor: pointer;
        display: flex;
        align-items: center;
        gap: 5px;
        font-size: ${props => props.theme.extraSmall};
      }
    }
  }
`

export default Archives;
