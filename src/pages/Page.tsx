import {
    IonContent,
    IonPage
} from "@ionic/react";
import React from "react";
import {useParams} from "react-router";
import ExploreContainer from "../components/ExploreContainer";
import TabRootPage from "./TabRootPage";
import OpenHeader from "../components/OpenHeader";
import styled from "styled-components";
import burgerMenu from "../assets/menu.svg";

const Page: React.FC = () => {
    const {name} = useParams<{ name: string }>();
    return (
            <IonPage>
                <OpenHeader icon={burgerMenu}/>
                <MenuContent>
                    <div className="title">
                        <p>Paramètres</p>
                    </div>
                    <Content>
                        <div className="menu__content container">
                            <ExploreContainer name={name}/>
                            <TabRootPage/>
                        </div>
                    </Content>
                </MenuContent>
            </IonPage>
    );
};

const MenuContent = styled(IonContent)`
  --overflow: hidden;

  .no-padding {
    padding: 0;
  }

  .title {
    width: 85%;
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }
`

const Content = styled("div")`
  margin-top: 20px;
  height: 100vh;
  background: ${props => props.theme.gradientPrimary};
  color: ${props => props.theme.white};
  border-radius: 20px 20px 0 0;
  padding: 15px 0;

  .menu__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};

    &:last-of-type {
      margin-top: 30px;
    }
  }
`

export default Page;
