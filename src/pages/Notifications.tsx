import {
    IonContent,
    IonItem,
    IonPage,
    IonToggle,
    IonLabel
} from "@ionic/react";
import {
    notifications,
    timeOutline
} from 'ionicons/icons';
import React, {useState} from "react";
import TabRootPage from "./TabRootPage";
import OpenHeader from "../components/OpenHeader";
import styled from "styled-components";
import IconLabelInligne from "../components/IconLabelInligne";
import arrowBack from "../assets/arrow-back-outline.svg";
import burgerMenu from "../assets/menu.svg";

// TODO: Front
const Notifications: React.FC = () => {
    const [checked, setChecked] = useState(false);
    return (
        <IonPage>
            <OpenHeader icon={burgerMenu}/>
            <MenuContent>
                <div className="title">
                    <p>Paramètres</p>
                </div>
                <Content>
                    <div className="menu__content container">
                        <p className="menu__subtitle">Notifications et rappels</p>
                        <MenuItem className="notifications__switch">
                            <IconLabelInligne icon={notifications} label="Rappels"/>
                            <IonToggle checked={checked} onIonChange={(e) => setChecked(e.detail.checked)}/>
                        </MenuItem>
                        <MenuItem className="notifications__alarm">
                            <IconLabelInligne icon={timeOutline} label="Heure du rappel"/>
                            <p>18h30</p>
                        </MenuItem>
                    </div>
                </Content>
            </MenuContent>
        </IonPage>
    );
};

const MenuContent = styled(IonContent)`
  --overflow: hidden;
  position: relative;
  .no-padding {
    padding: 0;
  }
  
  .title {
    width: 85%;
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }
`

const Content = styled("div")`
  margin-top: 20px;
  height: 100vh;
  background: ${props => props.theme.gradientPrimary};
  color: ${props => props.theme.white};
  border-radius: 20px 20px 0 0;
  padding: 15px 0;

  .menu__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};
  }
  
  .notifications__switch {
    display: flex;
    width: 100%;
    justify-content: space-between;
  }
`
const MenuItem = styled(IonItem)`
  --ion-background-color: none;
  --padding-start: 0;
  --inner-border-width: 0;
  color: ${props => props.theme.white};
  margin: 10px 0;
  &:hover {
    --color: ${props => props.theme.white};
  }
`

export default Notifications;
