import {IonContent, IonPage, IonIcon} from '@ionic/react';
import {
    chevronForwardOutline,
    chevronBackOutline,
    addOutline
} from 'ionicons/icons';

import React, {useState} from "react";
import Gender from "../assets/identity-rafiki.png";
import Analyze from "../assets/analysis-rafiki.png";
import Progress from "../assets/overview-amico.png";
import Sickeness from "../assets/hypochondriac-amico.png";
import {Button} from "../components/Button";
import styled from "styled-components";
import Modal from "../components/Modal";
import useModal from "../hooks/useModal";


const Onboarding: React.FC = () => {
    const { isShowing, toggle } = useModal();

    const contents = [
        {
            imageUrl: Gender,
            title: 'Suivez votre transit au quotidien',
            desc: 'Enregistrer chacune de vos selles à chaque passage au petit coin et profitez d\'un calendrier détaillé et un historique fiable'
        },
        {
            imageUrl: Analyze,
            title: 'Gardez un oeil sur votre santé intestinale',
            desc: 'Belly Buddy analyse vos selles chaque semaine pour vous donnez un pronostique concret sur votre santé gastrique'
        },
        {
            imageUrl: Progress,
            title: 'Améliorez votre bien-être grâce à nos solutions personnalisées',
            desc: 'Belly Buddy vous propose également des solutions adaptées à votre situation afin de rendre votre quotidien plus agréable'
        },
        {
            imageUrl: Sickeness,
            title: 'Avez-vous des maladies (MICI), des troubles métaboliques ou encore digestifs ?'
        }
    ];

    const [displayedContent, setDisplayedContent] = useState(contents[0]);
    const [index, setIndex] = useState(0);

    const HandleClickNext = () => {
        // Searching the object displayedContent inside the array "contents" to know his index
        let nextIndex: number = contents.findIndex(item => item.title === displayedContent.title);

        if (nextIndex + 1 < contents.length) {
            nextIndex += 1;
        }
        setDisplayedContent(contents[nextIndex]);
        setIndex(nextIndex);
    }

    const HandleClickPrev = () => {
        let prevIndex: number = contents.findIndex(item => item.title === displayedContent.title);

        if (prevIndex - 1 < contents.length) {
            prevIndex -= 1;
        }
        setDisplayedContent(contents[prevIndex]);
        setIndex(prevIndex);
    }


    return (
        <Content>
            <IonContent fullscreen>
                <IonContent className="onboarding__content">
                    <div key={displayedContent.imageUrl} className="flex-start container">
                        <Modal isShowing={isShowing} hide={toggle} title="Nouveau"/>
                        <img src={displayedContent.imageUrl} alt="" className="onboarding__image"/>
                        <h3 className="onboarding__title">{displayedContent.title}</h3>
                        <p className="onboarding__desc">{displayedContent.desc}</p>
                        {index === 3 &&
                                <Button
                                    className="modal-toggle"
                                    as='button'
                                    styleType='button--rounded'
                                    onClick={(evt) => toggle()}
                                >
                                    <IonIcon icon={addOutline}></IonIcon>
                                    Ajouter
                                </Button>
                        }
                    </div>
                    <div className="flex-end container">
                        <div className="onboarding__index">
                            {contents.map((content, index) => (
                                content.title === displayedContent.title
                                    ? <span className="active"></span>
                                    : <span></span>
                            ))}
                        </div>
                        <div className="onboarding__nav">
                            {index === 0 &&
                            <div className="align-end">
                                <Button
                                    as='button'
                                    styleType='button--primary'
                                    onClick={(evt) => HandleClickNext()}
                                >
                                    Suivant
                                    <IonIcon icon={chevronForwardOutline}></IonIcon>
                                </Button>
                            </div>
                            }
                            {index > 0 && index < 3 &&
                            <>
                                <Button
                                    as='button'
                                    styleType='button--grey'
                                    onClick={(evt) => HandleClickPrev()}
                                >
                                    <IonIcon icon={chevronBackOutline}></IonIcon>
                                    Précédent
                                </Button>
                                <Button
                                    as='button'
                                    styleType='button--primary'
                                    onClick={(evt) => HandleClickNext()}
                                >
                                    Suivant
                                    <IonIcon icon={chevronForwardOutline}></IonIcon>
                                </Button>
                            </>
                            }
                            {index === 3 &&
                                <>
                                    <Button
                                        as='button'
                                        styleType='button--grey'
                                        onClick={(evt) => HandleClickPrev()}
                                    >
                                        <IonIcon icon={chevronBackOutline}></IonIcon>
                                        Précédent
                                    </Button>
                                    <Button
                                        as='link'
                                        // 'to' is required since it's required in the Link component
                                        to='/tabs/homepage'
                                        styleType='button--primary'
                                        onClick={(evt) => {
                                            // evt should be of type React.MouseEvent<HTMLAnchorElement, MouseEvent>
                                            console.log(evt)
                                        }}
                                    >
                                        Terminer
                                    </Button>

                                </>
                            }
                        </div>
                    </div>
                </IonContent>
            </IonContent>
        </Content>
    );
};

const Content = styled(IonPage)`
  --overflow: hidden;
  display: flex;
  flex-direction: column;
  align-items: center;
  text-align: center;
  
  button {
    display: flex;
    gap: 5px;
  }

  .align-end {
    display: flex;
    width: 100%;
    justify-content: flex-end;
  }

  .flex-start {
    padding-top: 25px;
    height: calc(70% - 25px);
    display: flex;
    flex-direction: column;
    align-items: center;
  }

  .flex-end {
    padding-bottom: 25px;
    height: calc(30% - 25px);
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }

  .onboarding__image {
    width: 80%;
    margin-bottom: 50px;
  }

  .onboarding__title {
    font-size: ${props => props.theme.large};
    font-family: ${props => props.theme.sofiaProSemiBold};
    margin-bottom: 50px;
  }
  
  .onboarding__nav {
    display: flex;
    justify-content: space-between;
    align-items: normal;
    margin-top: 50px;
  }

  .onboarding__index {
    display: flex;
    justify-content: center;
    gap: 12px;

    span {
      height: 12px;
      width: 12px;
      background: ${props => props.theme.lightGrey};
      border-radius: 20px;

      &.active {
        background: ${props => props.theme.primary};
      }
    }
  }
`

export default Onboarding;
