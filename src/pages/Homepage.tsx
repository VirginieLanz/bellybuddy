import {IonContent, IonPage} from '@ionic/react';
import React, {useEffect, useState} from "react";
import OpenHeader from "../components/OpenHeader";
import {Storage} from '@capacitor/storage';
import styled from 'styled-components';
import littlePain from "../assets/little-pain.svg";
import moderatePain from "../assets/moderate-pain.svg";
import strongPain from "../assets/strong-pain.svg";
import ResumeSession from "../components/ResumeSession";
import noData from "../assets/null.svg";
import burgerMenu from "../assets/menu.svg";
import FirstSessionWC from "../components/FirstSessionWC";
// @ts-ignore
import SessionResultBlock from "../components/SessionResultBlock";
import SessionBadResultBlock from "../components/SessionBadResultBlock";
import PainResultBlock from "../components/PainResultBlock";
import NoSessionResultBlock from "../components/NoSessionResultBlock";
import NoSessionResultImageBlock from "../components/NoSessionResultImageBlock";


//TODO: Ajouter l'animation des douleurs quand les sessions sont vides
const Homepage: React.FC = () => {
    // Tableau des sessions WC
    let sessionArray: any[] = [];
    // Tableau des sessions WC enregistrées durant les 7 derniers jours
    let sessionArray7: any[] = [];


    const [numberOfSessions, setNumberOfSessions] = useState<number>(0);
    const [duration, setDuration] = useState<string>();
    const [stoolType, setStoolType] = useState<string>();
    const [pain, setPain] = useState<string>();
    const [sessionsPerDay, setSessionsPerDay] = useState<number>();


    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});

        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
        }
    };


    /**
     * Calcul la moyenne du nombre de sessions durant les 7 derniers jours
     */
    const calculateAverageNumberOfSessionsPerDay = () => {
        let total: number = sessionArray7.length / 7;

        // Arrondi à 1 décimale après la virgule
        total = Math.round(total * 10) / 10;

        setSessionsPerDay(total);
    }

    /**
     * Calcul la moyenne des douleurs des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAveragePain = () => {
        // Variable qui va contenir le nombre de chaque douleur
        let numberOfPainOccurences = {
            "Pas ou peu de douleurs": 0,
            "Modérément": 0,
            "Fortement": 0,
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let session in sessionArray7) {
            // On récupère la douleur et on l'incrémente
            // @ts-ignore
            numberOfPainOccurences[sessionArray7[session].pain.toString()] = numberOfPainOccurences[sessionArray7[session].pain.toString()] + 1;
        }

        // Variable qui set par défaut le type de douleur le plus fréquent à "< 3"
        let mostFrequentPain: string = "Pas ou peu de douleurs";

        // Récupère le type de douleur le plus fréquent
        for (let pain in numberOfPainOccurences) {
            // @ts-ignore
            if (numberOfPainOccurences[mostFrequentPain] < numberOfPainOccurences[pain]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentPain = pain;
            }
        }

        setPain(mostFrequentPain);
    }

    /**
     * Calcul la moyenne des types de selles des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAverageType = () => {
        // Variable qui va contenir le nombre de chaque durée
        let numberOfTypeOccurences = {
            "Type 1": 0,
            "Type 2": 0,
            "Type 3": 0,
            "Type 4": 0,
            "Type 5": 0,
            "Type 6": 0,
            "Type 7": 0
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let session in sessionArray7) {
            // On récupère le type et on l'incrémente
            // @ts-ignore
            numberOfTypeOccurences[sessionArray7[session].stool.toString()] = numberOfTypeOccurences[sessionArray7[session].stool.toString()] + 1;
        }

        // Variable qui set par défaut la durée la plus fréquente à "< 3"
        let mostFrequentStool: string = "Type 1";

        // Récupère la durée la plus fréquente
        for (let type in numberOfTypeOccurences) {
            // @ts-ignore
            if (numberOfTypeOccurences[mostFrequentStool] < numberOfTypeOccurences[type]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentStool = type;
            }
        }

        setStoolType(mostFrequentStool);
    }


    /**
     * Calcul la moyenne de la durée de selles des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAverageDuration = () => {
        // Variable qui va contenir le nombre de chaque durée
        let numberOfDurationOccurences = {
            "< 3 minutes": 0,
            "3 à 5 minutes": 0,
            "> 5 minutes": 0,
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let session in sessionArray7) {
            // On récupère la durée et on l'incrémente
            // @ts-ignore
            numberOfDurationOccurences[sessionArray7[session].duration.toString()] = numberOfDurationOccurences[sessionArray7[session].duration.toString()] + 1;
        }

        // Variable qui set par défaut la durée la plus fréquente à "< 3"
        let mostFrequentDuration: string = "< 3 minutes";

        // Récupère la durée la plus fréquente
        for (let duration in numberOfDurationOccurences) {
            // @ts-ignore
            if (numberOfDurationOccurences[mostFrequentDuration] < numberOfDurationOccurences[duration]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentDuration = duration;
            }
        }

        setDuration(mostFrequentDuration);
    }


    /**
     * Met à jour le tableau sessionArray7
     */
    const getSessionForTheLast7Days = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let sevenDaysAgoDate = currentDate.getDate() - 7; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de sevenDaysAgoDate
            if (new Date(sessionArray[i].date).getDate() >= sevenDaysAgoDate) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArray7.push(sessionArray[i]);
            } else {
                break;
            }
        }
    }


    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            console.log(sessionArray.length)
            setNumberOfSessions(sessionArray.length);
            // On récupère le tableau contenant les sessions durant les 7 derniers jours
            getSessionForTheLast7Days();

            // On récupère le type de douleur le plus courant pour l'afficher
            calculateAveragePain();

            // On récupère le type le plus courant pour l'afficher
            calculateAverageType();

            // On récupère la durée la plus courante pour l'afficher
            calculateAverageDuration();

            // On récupère le nombre moyen de sessions durant les 7 derniers jours
            calculateAverageNumberOfSessionsPerDay();
        })
        return () => {
        };
    }, []);


    return (
        <IonPage>
            <OpenHeader icon={burgerMenu}/>
            <Content>
                <div className="title">
                    <p><span className="title__primary">Gardez</span> un oeil sur</p>
                    <p><span className="title__primary">votre santé</span> intestinale</p>
                </div>

                {numberOfSessions === 0 ?
                    <div className="resume__container no-data">
                        <FirstSessionWC/>
                        <p className="subtitle">7 derniers jours</p>
                        <div className="resume__content">
                            <Block className="resume__block small-width animation-pain">
                                <PainResultBlock styledClass="low-background" title="Douleurs" painImage={littlePain}/>
                            </Block>
                            <Block className="resume__block large-width">
                                <NoSessionResultBlock title="Type le plus fréquent" result="Données indisponibles"/>
                            </Block>
                        </div>
                        <div className="resume__content">
                            <Block className="resume__block large-width">
                                <NoSessionResultBlock title="Durée moyenne" result="Données indisponibles"/>
                            </Block>
                            <Block className="resume__block small-width">
                                <NoSessionResultImageBlock title="Fréquence moyenne" resultImage={noData}/>
                            </Block>
                        </div>
                    </div>
                    :
                    <div className="resume__container">
                        <p className="subtitle">7 derniers jours</p>
                        <div className="resume__content">
                            <Block className="resume__block small-width">
                                {pain === 'Pas ou peu de douleurs' &&
                                    <PainResultBlock styledClass="low-background" title="Douleurs" painImage={littlePain}/>
                                }
                                {pain === 'Modérément' &&
                                <PainResultBlock styledClass="moderate-background" title="Douleurs" painImage={moderatePain}/>
                                }
                                {pain === 'Fortement' &&
                                <PainResultBlock styledClass="strong-background" title="Douleurs" painImage={strongPain}/>
                                }
                            </Block>
                            <Block className="resume__block large-width">
                                {stoolType === 'Type 1' || stoolType === 'Type 7' ?
                                    <SessionBadResultBlock title="Type le plus fréquent" result={stoolType}/>
                                    : stoolType === 'Type 3' || stoolType === 'Type 4' ?
                                    <SessionResultBlock styledClass="low-border" title="Type le plus fréquent" result={stoolType}/>
                                    :
                                    <SessionResultBlock styledClass="moderate-border" title="Type le plus fréquent" result={stoolType}/>
                                }
                            </Block>
                        </div>
                        <div className="resume__content">
                            <Block className="resume__block large-width">
                                {duration === '< 3 minutes' ?
                                <SessionResultBlock styledClass="low-border" title="Durée moyenne" result={duration}/>
                                : duration === '3 à 5 minutes' ?
                                <SessionResultBlock styledClass="moderate-border" title="Durée moyenne" result={duration}/>
                                :
                                <SessionBadResultBlock title="Durée moyenne" result={duration}/>
                                }
                            </Block>
                            <Block className="resume__block small-width">
                                <SessionResultBlock styledClass="" title="Fréquence moyenne" result={sessionsPerDay}/>
                            </Block>
                        </div>
                        <p className="subtitle margint-20">Votre dernière session WC enregistrée</p>
                        <div className="resume__content last-session">
                            <ResumeSession/>
                        </div>
                    </div>
                }
            </Content>
        </IonPage>
    );
};


const Content = styled(IonContent)`
  width: 85%;
  display: flex;
  align-self: center;
  --overflow: hidden;
  .no-padding {
    padding: 0;
  }

  .title {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px 0 40px 0;

    .title__primary {
      color: ${props => props.theme.primary};
    }
  }

  .subtitle {
    font-family: ${props => props.theme.sofiaProRegular};
    font-size: ${props => props.theme.small};
    margin-bottom: -10px;

    &.margint-20 {
      margin-top: 20px;
    }
  }

  .resume__container {
    margin-top: 20px;
    display: flex;
    flex-direction: column;
    gap: 20px;

    &.no-data {
      .resume__data {
        font-size: ${props => props.theme.medium};
      }

      .add-session__content {
        height: inherit;
      }
    }
  }

  .resume__content {
    display: flex;
    height: 130px;
    gap: 20px;

    &.last-session {
      height: 110px;
    }
    
    .large-width {
      width: 70%;
    }
    
    .small-width {
      width: 30%;
    }
  }
`


const Block = styled("div")`
  border-radius: 20px;
  display: flex;
  flex-direction: column;
  text-align: center;
  background: ${props => props.theme.lightGrey};

  .primary-background {
    background: ${props => props.theme.gradientPrimary};
  }
`
export default Homepage;
