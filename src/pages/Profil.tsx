import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';


interface PageProps {
    pageName: string | undefined;
}

const Profil: React.FC<PageProps> = ({ pageName }) => {

    return (
        <IonPage>
            <IonHeader>
                <IonToolbar>
                    <IonTitle>{pageName}</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent fullscreen>
                <IonHeader collapse="condense">
                    <IonToolbar>
                        <IonTitle size="large">Tab 4</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <ExploreContainer name="Tab 4 page" />
            </IonContent>
        </IonPage>
    );
};



export default Profil;
