import React from "react";
import styled from "styled-components";
import arrowDownGrey from "../assets/arrowDownGrey.svg";

interface ContainerProps {
    title: string;
    children: any;
}

const AccordionDesc: React.FC<ContainerProps> = ({ title, children}) => {
    const [isOpen, setOpen] = React.useState(false);
    return (
        <Content>
            <div className={`accordion-desc__container ${isOpen ? "border" : ""}`}>
                <div className={`accordion-desc__title ${isOpen ? "open" : ""}`}>
                    <p>
                        {title}
                    </p>
                    <div className="accordion__icon">
                        <img onClick={() => setOpen(!isOpen)} src={arrowDownGrey} alt=""/>
                    </div>
                </div>

                <div className={`accordion-desc__item ${!isOpen ? "collapsed" : ""}`}>
                    <div className="accordion-desc__content">{children}</div>
                </div>
            </div>
        </Content>
    );
};


const Content = styled("div")`
  .accordion-desc__title {
    color: ${props => props.theme.darkGrey};
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 20px;
    width: 100%;
    font-family: ${props => props.theme.sofiaProMedium};

    img {
      cursor: pointer;
      min-width: 15px;
      width: 15px;
    }
  }

  .accordion-desc__item {
    overflow: hidden;
    transition: max-height 0.3s cubic-bezier(1, 0, 1, 0);
    height: auto;
    max-height: 9999px;

    &.collapsed {
      max-height: 0;
      transition: max-height 0.35s cubic-bezier(0, 1, 0, 1);
    }
  }
  
  .accordion-desc__content {
    margin-bottom: 10px;
    
    &:first-child {
      margin-top: 10px;
    }
    ul, li, p {
      font-family: ${props => props.theme.sofiaProLight};
      font-size: ${props => props.theme.extraSmall};
      line-height: initial;
    }
    
    ul {
      margin-bottom: 10px;
      li {
        padding-left: 10px;
      }
    }
  }

`



export default AccordionDesc;
