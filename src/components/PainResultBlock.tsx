import React from "react";
import styled from "styled-components";
interface ContainerProps {
    styledClass: string;
    title: string;
    painImage: string,
}

const PainResultBlock: React.FC<ContainerProps> = ({ styledClass, title, painImage}) => {

    return (
        <Content className={`block content ${styledClass}`}>
            <p className="resume__title">{title}</p>
            <div className="resume__pain">
                <img src={painImage} alt=""/>
            </div>
        </Content>
    );
};


const Content = styled("div")`
  height: 100%;
  border-radius: 20px;

  &.low-background {
    background: ${props => props.theme.gradientForth};
  }

  &.moderate-background {
    background: ${props => props.theme.gradientThird};
  }

  &.strong-background {
    background: ${props => props.theme.gradientSecondary};
  }

  .primary-background {
    background: ${props => props.theme.gradientPrimary};
  }

  .resume__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    line-height: 120%;
    height: 10%;
  }


  .resume__data {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    height: calc(90% - 15px);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 20px;
  }
  
  &.content {
    color: ${props => props.theme.white};
    width: 100%;

    .resume__pain {
      height: 90%;
      display: flex;
      align-items: center;
      justify-content: center;
    }

    img {
      width: 60px;
    }
  }
`



export default PainResultBlock;
