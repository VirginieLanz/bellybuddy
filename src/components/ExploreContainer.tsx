import React from "react";
import {useLocation} from 'react-router-dom';
import styled from "styled-components";
import {
    Accordion,
    AccordionItem,
    AccordionItemButton,
    AccordionItemHeading,
    AccordionItemPanel,
} from 'react-accessible-accordion';
import {addOutline} from 'ionicons/icons';
import {IonIcon} from "@ionic/react";

interface ContainerProps {
    name: string;
}

const ExploreContainer: React.FC<ContainerProps> = ({name}) => {
    const usePathName = () => {
        const location = useLocation();
        return location.pathname;
    }
    const pathname = window.location.pathname;
    console.log(pathname);

    const items = [
        {
            title: 'Pourquoi utiliser Belly Buddy ?',
            content: 'Pour améliorer vote bien-être physique et psychique.'
        },

        {
            title: 'Est-ce que mes données sont confidentielles ?',
            content: 'Toutes les données sont stockées dans la mémoire de votre application et sont confidentielles. Une fois l\'application désinstallée, toutes vos données seront perdues.'
        },

        {
            title: 'Quelles sont les options premium ?',
            content: 'L\'option premium permet l\'accès à l\'analyse par mois et par année de vos données ainsi que l\'accès à des solutions personnalisées selon votre dashboard.'
        },

        {
            title: 'Est-ce que je peux avoir plusieurs comptes ?',
            content: 'L\'application ne permet pas à ce jour d\'ajouter plusieurs utilisateurs sur le même téléphone mais nous souhaitons intégrer cette option prochainement.'
        },

        {
            title: 'Est-ce que je peux avoir plusieurs comptes ?',
            content: 'L\'application ne permet pas à ce jour d\'ajouter plusieurs utilisateurs sur le même téléphone mais nous souhaitons intégrer cette option prochainement.'
        }
    ]

    return (
        <>
            {pathname === '/page/profil' && <>Profil</>}
            {pathname === '/page/rappels' && <>rappels</>}
            {pathname === '/page/faq' &&
            <Content>
                <div className="menu__content pages">
                    <p className="page__title container">Foire aux questions</p>
                    <p className="page__subtitle container">Les questions les plus fréquemment posées</p>
                    <div className="accordions">
                        <Accordion>
                            {items.map((item, index) => (
                                <AccordionItem key={index}>
                                    <AccordionItemHeading>
                                        {item.title}
                                        <AccordionItemButton>
                                            <IonIcon icon={addOutline}></IonIcon>
                                        </AccordionItemButton>
                                    </AccordionItemHeading>
                                    <AccordionItemPanel>
                                        {item.content}
                                    </AccordionItemPanel>
                                </AccordionItem>
                            ))}
                        </Accordion>
                    </div>
                </div>
            </Content>
            }
            {pathname === '/page/expertise' && <>expertise</>}
        </>
    );
};

const Content = styled("div")`
  .accordions {
    background: ${props => props.theme.white};
    padding: 20px;
    border-radius: 20px;
    min-height: 100%;
    color: ${props => props.theme.darkGrey};
    font-family: ${props => props.theme.sofiaProRegular};
    font-size: ${props => props.theme.small};
    width: 80%;
    margin: 0 auto;
    margin-top: 10px;

    .accordion__item {
      padding: 10px 0;
      border-bottom: 1px solid ${props => props.theme.primary};
    }
    
    .accordion__heading {
      display: flex;
      flex-direction: row;
      justify-content: space-between;
    }
    
    .accordion__button {
      cursor: pointer;
      color: ${props => props.theme.primary};
    }
    
    .accordion__panel {
      padding-top: 10px;
      color: ${props => props.theme.primary};
    }
  }
  
  .pages {
    position: relative;
    z-index: 999;
    
    p {
      font-size: ${props => props.theme.small};
    }
  
    .page__title {
      font-family: ${props => props.theme.sofiaProSemiBold};
    }
    .page__subtitle {
      font-family: ${props => props.theme.sofiaProRegular};
      margin: 20px auto 5px auto;
    }
  }
`

export default ExploreContainer;
