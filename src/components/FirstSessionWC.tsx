import React from "react";
import styled from "styled-components"
import {NavLink} from "react-router-dom";
import addSession from "../assets/add.svg";

const FirstSessionWC: React.FC = () => {
    return (
        <Content className="resume__session add-session">
            <NavLink to="/wc-session">
                <img src={addSession} alt=""/>
            </NavLink>
            <p>Démarrer ma première session WC</p>
        </Content>
    );
};

const Content = styled("div")`
  display: flex;
  gap: 5px;
  align-items: center;
  width: 90%;
  border-radius: 20px;
  padding: 12px 16px;
  font-family: ${props => props.theme.sofiaProMedium};
  font-size: ${props => props.theme.small};
  color: ${props => props.theme.white};

  &.add-session {
    background: ${props => props.theme.primary};
    color: ${props => props.theme.white};

    a {
      min-height: 51px;
      min-width: 51px;
      height: auto;
      display: flex;
      justify-content: center;
    }
    
    img {
      height: 100%;
    }

    p {
      text-align: center;
      padding: 0 20px;
      margin: 0 auto;
      font-family: ${props => props.theme.sofiaProSemiBold};
    }
  }
`

export default FirstSessionWC;
