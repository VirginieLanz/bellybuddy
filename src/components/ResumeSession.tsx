import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import styled from "styled-components"
import {Storage} from "@capacitor/storage";
import littlePain from "../assets/little-pain.svg";
import moderatePain from "../assets/moderate-pain.svg";
import strongPain from "../assets/strong-pain.svg";
import {IonIcon} from "@ionic/react";
import {arrowForwardOutline} from 'ionicons/icons';
//TODO: Terminer ce composant
//TODO: Ajouter lien archive détail

const ResumeSession: React.FC = () => {
    let sessionArray: any[] = [];
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [lastSessionData, setLastSessionData] = useState<any>({});
    let index = sessionArray.length;
    let history = useHistory();

    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});
        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
        }
    };


    /**
     * Récupère la dernière session enregistrée
     */
    const getLastSession = () => {
        if (lastSessionData !== undefined) {
            setLastSessionData(sessionArray[sessionArray.length - 1]);
        } else {
            return 0;
        }
    }


    // eslint-disable-next-line react-hooks/rules-of-hooks
    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            // On récupère la dernière session enregistrée
            getLastSession();
        })
        return () => {
        };
    }, []);

    return (
        <Content
            className={`resume__session ${lastSessionData?.pain === 'Modérément' ? "moderate-pain" : lastSessionData?.pain === 'Fortement' ? "strong-pain" : "low-pain"}`}>
            <div className="session__data">
                <div className="data--top">
                    <div className="data--top--left">
                        <p>{lastSessionData?.hour}</p>
                        <p>Durée : <span>{lastSessionData?.duration}</span></p>
                        <p>Type : <span>{lastSessionData?.stool}</span></p>
                    </div>
                    <div className="data--top--right">
                        {lastSessionData?.pain === 'Fortement' &&
                        <img src={strongPain} alt=""/>
                        }
                        {lastSessionData?.pain === 'Modérément' &&
                        <img src={moderatePain} alt=""/>
                        }
                        {lastSessionData?.pain === 'Pas ou peu de douleurs' &&
                        <img src={littlePain} alt=""/>
                        }
                    </div>
                </div>
                <div className="data--bottom">
                    <p onClick={() => history.push({
                        pathname: '/archive/details',
                        state: lastSessionData
                    })}>
                        Voir les détails <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                </div>
            </div>
        </Content>
    );
};

const Content = styled("div")`
  display: flex;
  gap: 5px;
  align-items: center;
  width: 100%;
  border-radius: 20px;
  padding: 12px 16px;
  font-family: ${props => props.theme.sofiaProMedium};
  font-size: ${props => props.theme.small};
  color: ${props => props.theme.white};

  &.moderate-pain {
    background: ${props => props.theme.gradientThird};
  }

  &.strong-pain {
    background: ${props => props.theme.gradientSecondary};
  }

  &.low-pain {
    background: ${props => props.theme.gradientForth};
  }

  .session__data {
    width: 100%;
    height: 100%;
    line-height: 130%;
    display: flex;
    flex-direction: column;
    justify-content: space-between;

    .data--top {
      display: flex;
      width: 100%;
      justify-content: space-between;

      .data--top--right {
        padding-top: 5px;
      }
    }

    .data--bottom {
      display: flex;
      justify-content: flex-end;

      p {
        cursor: pointer;
        display: flex;
        align-items: center;
        gap: 5px;
        font-size: ${props => props.theme.extraSmall};
      }
    }
  }
`

export default ResumeSession;
