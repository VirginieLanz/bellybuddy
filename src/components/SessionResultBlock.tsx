import React from "react";
import styled from "styled-components";
interface ContainerProps {
    styledClass: string;
    title: string;
    result: any,
}

const SessionResultBlock: React.FC<ContainerProps> = ({ styledClass, title, result}) => {

    return (
        <Content className={`block ${styledClass}`}>
            <p className="resume__title">{title}</p>
            <p className="resume__data">{result}</p>
        </Content>
    );
};


const Content = styled("div")`
  min-height: 130px;
  height: auto;
  border-radius: 20px;

  &.low-border {
    border: 1px solid ${props => props.theme.forth};
    box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
    -webkit-box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
    -moz-box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
  }

  &.moderate-border {
    border: 1px solid ${props => props.theme.third};
    box-shadow: 1px 4px 10px 0px rgba(255, 216, 79, 0.31);
    -webkit-box-shadow: 1px 4px 10px 0px rgba(255, 216, 79, 0.31);
    -moz-box-shadow: 1px 4px 10px 0px rgba(255, 216, 79, 0.31);
  }

  &.strong-border {
    border: 1px solid ${props => props.theme.secondary};
    box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
    -webkit-box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
    -moz-box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
  }
  
  &.notes {
    min-height: 130px;
    height: min-content;
    .resume__data {
      font-family: ${props => props.theme.sofiaProLight};
      font-size: ${props => props.theme.small};
    }
  }

  .resume__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    line-height: 120%;
    height: 30px;
  }


  .resume__data {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    height: calc(100% - 70px);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 20px;
  }
`



export default SessionResultBlock;
