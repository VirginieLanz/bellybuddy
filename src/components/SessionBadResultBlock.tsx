import React from "react";
import styled from "styled-components";
import tips from "../assets/tips.svg";
import {NavLink} from "react-router-dom";

// @ts-ignore

interface ContainerProps {
    title: string;
    result: any;
}

const SessionBadResultBlock: React.FC<ContainerProps> = ({ title, result}) => {

    return (
        <Content className="block strong-border">
            <p className="resume__title">{title}</p>
            <p className="resume__data tips">{result}</p>
            <div className="resume__tips">
                <NavLink to="/tabs/advices">
                    <img src={tips} alt=""/>
                </NavLink>
            </div>
        </Content>
    );
};


const Content = styled("div")`
  min-height: 130px;
  height: auto;
  border-radius: 20px;
  
  &.strong-border {
    border: 1px solid ${props => props.theme.secondary};
    box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
    -webkit-box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
    -moz-box-shadow: 1px 4px 10px 0px rgba(255, 195, 189, 0.31);
  }
  
  .resume__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    line-height: 120%;
    height: 30px;
  }

  .resume__data {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    height: calc(100% - 70px);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 20px;

    &.tips {
      flex-wrap: wrap;
      gap: 10px;
    }
  }

  .resume__tips {
    display: flex;
    justify-content: flex-end;
    padding-right: 10px;
    height: 30px;
  }
`



export default SessionBadResultBlock;
