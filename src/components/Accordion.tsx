import React from "react";
import styled from "styled-components";
import arrowDown from "../assets/arrowDown.svg";

interface ContainerProps {
    styledClass: string;
    title: string;
    children: any;
}

const Accordion: React.FC<ContainerProps> = ({ styledClass, title, children}) => {
    const [isOpen, setOpen] = React.useState(false);
    return (
        <Content className={styledClass}>
            <div className={`accordion__container ${isOpen ? "border" : ""}`}>
                <div className={`accordion__title ${isOpen ? "open" : ""}`}>
                    <p>
                        {title}
                    </p>
                    <div className="accordion__icon">
                        <img onClick={() => setOpen(!isOpen)} src={arrowDown} alt=""/>
                    </div>
                </div>

                <div className={`accordion__item ${!isOpen ? "collapsed" : ""}`}>
                    <div className="accordion__content">{children}</div>
                </div>
            </div>
        </Content>
    );
};


const Content = styled("div")`
  border-radius: 20px;
  width: 95%;
  margin: 0 auto;
  
  .accordion__paragraph {
    font-family: ${props => props.theme.sofiaProMedium};
    line-height: initial;
  }
  &.primary {
    background: ${props => props.theme.gradientPrimary};
    .accordion__container {
      &.border {
        .accordion__item {
          border: 1px solid ${props => props.theme.primary};
          box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
          -webkit-box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
          -moz-box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
        }
      }
    }
  }
  
  &.forth {
    background: ${props => props.theme.gradientForth};
    .accordion__container {
      &.border {
        .accordion__item {
          border: 1px solid ${props => props.theme.forth};
          box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
          -webkit-box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
          -moz-box-shadow: 1px 4px 10px 0px rgba(121, 234, 193, 0.31);
        }
      }
    }
  }
  
  .accordion__container {
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    align-items: center;
    color: ${props => props.theme.white};
    
    &.border {
      .accordion__item {
        border: 1px solid ${props => props.theme.primary};
        box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
        -webkit-box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
        -moz-box-shadow: 1px 4px 10px 0 rgba(64, 123, 255, 1);
      }
    }
  }
  .accordion__item {
    overflow: hidden;
    transition: max-height 0.3s cubic-bezier(1, 0, 1, 0);
    height: auto;
    max-height: 9999px;
    background: ${props => props.theme.white};
    border-radius: 20px;
    color: ${props => props.theme.darkGrey};
    
    &.collapsed {
      max-height: 0;
      transition: max-height 0.35s cubic-bezier(0, 1, 0, 1);
    }
  }


  .accordion__title {
    display: flex;
    justify-content: space-between;
    align-items: center;
    gap: 20px;
    width: 85%;
    font-family: ${props => props.theme.sofiaProSemiBold};
    padding: 20px 0;
    
    img {
      cursor: pointer;
      min-width: 15px;
      width: 15px;
    }
  }

  .accordion__content {
    padding: 20px 0;
    width: 85%;
    margin: 0 auto;
    display: flex;
    flex-direction: column;
    gap: 15px;
  }

`



export default Accordion;
