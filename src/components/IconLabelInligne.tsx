import {IonIcon} from "@ionic/react";
import React from "react";
import styled from "styled-components"

interface ContainerProps {
    icon: string;
    label: string;
}

const IconLabelInligne: React.FC<ContainerProps> = ({icon, label}) => {
    return (
        <Content className="icon-title">
            <MenuIcon icon={icon}/>
            <p>{label}</p>
        </Content>
    );
};

const Content = styled("div")`
  display: flex;
  gap: 5px;
  align-items: center;
  &:hover {
    color: ${props => props.theme.white};
  }
`

const MenuIcon = styled(IonIcon)`
  border: 1px solid ${props => props.theme.white};
  --white-rgb: 255, 255, 255;
  background: rgba(var(--white-rgb), .4);
  border-radius: 10px;
  padding: 5px;
  margin-right: 5px;
`



export default IconLabelInligne;
