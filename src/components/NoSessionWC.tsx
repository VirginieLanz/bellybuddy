import React from "react";
import styled from "styled-components"
import {NavLink} from "react-router-dom";
import addSession from "../assets/add.svg";
import noSession from "../assets/null-session.svg";

const NoSessionWC: React.FC = () => {
    return (
        <Content className="resume__session no-session">
            <img src={noSession} alt=""/>
            <p>Pas de session WC</p>
        </Content>
    );
};

const Content = styled("div")`
  display: flex;
  gap: 5px;
  align-items: center;
  width: 90%;
  border-radius: 20px;
  padding: 12px 16px;
  font-family: ${props => props.theme.sofiaProMedium};
  font-size: ${props => props.theme.small};
  color: ${props => props.theme.white};

  &.no-session {
    background: ${props => props.theme.mediumGrey};
    color: ${props => props.theme.white};

    a {
      height: auto;
      display: flex;
      justify-content: center;
    }
    
    img {
      height: 100%;
    }

    p {
      text-align: center;
      padding: 0 20px;
      margin: 0 auto;
      font-family: ${props => props.theme.sofiaProSemiBold};
    }
  }
`

export default NoSessionWC;
