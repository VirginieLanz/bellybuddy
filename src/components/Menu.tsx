import {
    IonContent,
    IonItem,
    IonList,
    IonMenu,
    IonMenuToggle,
} from '@ionic/react';

import React from 'react';
import {useLocation} from 'react-router-dom';
import {
    person,
    notifications,
    informationCircle,
    school,
} from 'ionicons/icons';
import styled from 'styled-components';
import IconLabelInligne from "./IconLabelInligne";
import Header from "./Header";


interface AppPage {
    url: string;
    iosIcon: string;
    mdIcon: string;
    title: string;
}

const appPagesUser: AppPage[] = [
    {
        title: 'Profil',
        url: '/page/profil',
        iosIcon: person,
        mdIcon: person
    },
    {
        title: 'Notifications et rappels',
        url: '/page/rappels',
        iosIcon: notifications,
        mdIcon: notifications
    }

];

const appPagesInfos: AppPage[] = [
    {
        title: 'FAQ',
        url: '/page/faq',
        iosIcon: informationCircle,
        mdIcon: informationCircle
    },
    {
        title: 'Expertise',
        url: '/page/expertise',
        iosIcon: school,
        mdIcon: school
    }
];

const Menu: React.FC = () => {
    const location = useLocation();

    return (
        <MenuContainer contentId="main" type="overlay">
            <MenuContent>
                <IonList id="inbox-list" className="no-padding">
                    <Header/>
                    <div className="title">
                        <p>Paramètres</p>
                    </div>
                    <Content>
                        <div className="menu__content container">
                            <p className="menu__subtitle">Utilisateur</p>
                            {appPagesUser.map((appPageUser, index) => {
                                return (
                                    <IonMenuToggle key={index} autoHide={false}>
                                        <MenuItem className={location.pathname === appPageUser.url ? 'selected' : ''}
                                                  routerLink={appPageUser.url} routerDirection="none" lines="none"
                                                  detail={false}>
                                            <IconLabelInligne icon={appPageUser.iosIcon} label={appPageUser.title}/>
                                        </MenuItem>
                                    </IonMenuToggle>
                                );
                            })}
                            <p className="menu__subtitle">Informations</p>
                            {appPagesInfos.map((appPageInfos, index) => {
                                return (
                                    <IonMenuToggle key={index} autoHide={false}>
                                        <MenuItem className={location.pathname === appPageInfos.url ? 'selected' : ''}
                                                  routerLink={appPageInfos.url} routerDirection="none" lines="none"
                                                  detail={false}>
                                            <IconLabelInligne icon={appPageInfos.iosIcon} label={appPageInfos.title}/>
                                        </MenuItem>
                                    </IonMenuToggle>
                                );
                            })}
                        </div>
                    </Content>
                </IonList>
            </MenuContent>
        </MenuContainer>
    );
};

const MenuContainer = styled(IonMenu)`
  --width: 100%;
`

const MenuContent = styled(IonContent)`
  --overflow: hidden;

  .no-padding {
    padding: 0;
  }

  .title {
    width: 85%;
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin: 30px auto 40px auto;
  }
`

const Content = styled("div")`
  margin-top: 20px;
  height: 100vh;
  background: ${props => props.theme.gradientPrimary};
  color: ${props => props.theme.white};
  border-radius: 20px 20px 0 0;
  padding: 15px 0;

  .menu__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};

    &:last-of-type {
      margin-top: 30px;
    }
  }
`

const MenuItem = styled(IonItem)`
  --ion-background-color: none;
  --padding-start: 0;
  color: ${props => props.theme.white};
  margin: 10px 0;

  &:hover {
    --color: ${props => props.theme.white};
  }
`


export default Menu;