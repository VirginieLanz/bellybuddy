import React from "react";
import styled from "styled-components";
interface ContainerProps {
    title: string;
    result: any,
}

const NoSessionResultBlock: React.FC<ContainerProps> = ({ title, result}) => {

    return (
        <Content className="block">
            <p className="resume__title">{title}</p>
            <p className="resume__data">{result}</p>
        </Content>
    );
};


const Content = styled("div")`
  height: 100%;
  border-radius: 20px;

  .resume__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    line-height: 120%;
    height: 10%;
  }


  .resume__data {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    height: calc(90% - 15px);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 20px;
  }
`



export default NoSessionResultBlock;
