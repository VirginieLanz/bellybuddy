import React from "react";
import ReactDOM from "react-dom";
import styled from 'styled-components';
import {Button} from "./Button";

const Modal = ({isShowing, hide, title}) =>
    isShowing
        ? ReactDOM.createPortal(
        <>
            <Content className="modal-overlay container--inner">
                <div className="modal__wrapper">
                    <div className="modal">
                        <div className="modal__header">
                            <p>{title}</p>
                        </div>
                        <div className="modal-flex">
                            <input type="text" placeholder="Saisir ici"/>
                            <div className="buttons-container">
                                <Button
                                    className="modal-close-button"
                                    as='button'
                                    type="button"
                                    styleType='button--border-white'
                                    onClick={(evt) => hide()}
                                >
                                    Annuler
                                </Button>
                                <Button
                                    className="modal-close-button"
                                    as='button'
                                    type="button"
                                    styleType='button--border-white'
                                    onClick={(evt) => hide()}
                                >
                                    Ajouter
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Content>
        </>,
        document.body
        )
        : null;

const Content = styled("div")`
  color: ${props => props.theme.white};
  border-radius: 20px;
  padding: 20px;
  position: relative;
  background: ${props => props.theme.primary};
  text-align: center;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);
  
  
  .modal__header {
    font-size: ${props => props.theme.small};
    font-family: ${props => props.theme.sofiaProSemiBold};
  }
  
  .modal-flex {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
  }
  
  .buttons-container {
    display: flex;
    justify-content: space-between;
    width: 100%;
  }

  input {
    width: 80%;
    margin: 20px 0 30px 0;
    background: none;
    border: none;
    border-bottom: 1px solid ${props => props.theme.white};
    padding-bottom: 5px;
    font-size: ${props => props.theme.extraSmall};
  }
`

export default Modal;