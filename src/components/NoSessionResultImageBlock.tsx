import React from "react";
import styled from "styled-components";
import noData from "../assets/null.svg";
interface ContainerProps {
    title: string;
    resultImage: any,
}

const NoSessionResultImageBlock: React.FC<ContainerProps> = ({ title, resultImage}) => {

    return (
        <Content className="block">
            <p className="resume__title">{title}</p>
            <div className="resume__data">
                <img src={resultImage} alt=""/>
            </div>
        </Content>
    );
};


const Content = styled("div")`
  height: 100%;
  border-radius: 20px;

  .resume__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-top: 10px;
    line-height: 120%;
    height: 10%;
  }


  .resume__data {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    height: calc(90% - 15px);
    display: flex;
    align-items: center;
    justify-content: center;
    padding: 0 20px;
  }
`



export default NoSessionResultImageBlock;
