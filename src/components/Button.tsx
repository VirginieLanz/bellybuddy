import * as React from 'react'
import {Link} from 'react-router-dom'
import type {LinkProps} from 'react-router-dom'
import styled from 'styled-components'

type BaseProps = {
    children: React.ReactNode
    className?: string
    styleType: 'button--primary' | 'button--grey' | 'button--rounded' | 'button--border-white'
}

type ButtonAsButton = BaseProps &
    Omit<React.ButtonHTMLAttributes<HTMLButtonElement>, keyof BaseProps> & {
    as?: 'button'
}

type ButtonAsUnstyled = Omit<ButtonAsButton, 'as' | 'styleType'> & {
    as: 'unstyled'
    styleType?: BaseProps['styleType']
}

type ButtonAsLink = BaseProps &
    Omit<LinkProps, keyof BaseProps> & {
    as: 'link'
}

type ButtonAsExternal = BaseProps &
    Omit<React.AnchorHTMLAttributes<HTMLAnchorElement>, keyof BaseProps> & {
    as: 'externalLink'
}

type ButtonProps =
    | ButtonAsButton
    | ButtonAsExternal
    | ButtonAsLink
    | ButtonAsUnstyled

export function Button(props: ButtonProps): JSX.Element {
    const allClassNames = `${props.styleType ? props.styleType : ''} ${
        props.className ? props.className : ''
    }`

    if (props.as === 'link') {
        // don't pass unnecessary props to component
        const {className, styleType, as, ...rest} = props
        return <StyledLink className={allClassNames} {...rest} />
    } else if (props.as === 'externalLink') {
        const {className, styleType, as, ...rest} = props
        return (
            <a
                className={allClassNames}
                // provide good + secure defaults while still allowing them to be overwritten
                target='_blank'
                rel='noopener noreferrer'
                {...rest}
            >
                {props.children}
            </a>
        )
    } else if (props.as === 'unstyled') {
        const {className, styleType, as, ...rest} = props
        return <button className={className} {...rest} />
    } else {
        const {className, styleType, as, ...rest} = props
        return <StyledButton className={allClassNames} {...rest} />
    }
}

const StyledLink = styled(Link)`
  all: initial;
  padding: 12px 20px;
  border-radius: 12px;
  font-size: ${props => props.theme.small};
  font-family: ${props => props.theme.sofiaProLight};
  cursor: pointer;
  display: flex;
  align-items: center;
  gap: 5px;
  
  &.button--primary {
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
  }

  &.button--border-white {
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
    border: 1px solid ${props => props.theme.white};
    --white-rgb: 255, 255, 255;
    padding: 10px 25px;
  }
`

const StyledButton = styled("button")`
  all: initial;
  font-size: ${props => props.theme.small};
  font-family: ${props => props.theme.sofiaProLight};
  padding: 12px 20px;
  border-radius: 12px;
  display: flex;
  align-items: center;
  cursor: pointer;
  gap: 5px;
  height: min-content;
  

  &.button--primary {
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
  }
  
  &.button--grey {
    color: ${props => props.theme.darkGrey};
    background: ${props => props.theme.lightGrey};
  }

  &.button--rounded {
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
    padding: 7px 14px;
    border-radius: 50px;
  }

  &.button--border-white {
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
    border: 1px solid ${props => props.theme.white};
    --white-rgb: 255, 255, 255;
    padding: 10px 25px;
  }
`