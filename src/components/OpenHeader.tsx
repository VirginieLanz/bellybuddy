import {IonHeader, IonMenuButton} from '@ionic/react';
import React from 'react';
import styled from 'styled-components';

interface ContainerProps {
    icon: string;
}

const OpenHeader: React.FC<ContainerProps> = ({icon}) => {
    return (
        <MenuHeader>
            <IonHeader className="menu__header ion-no-border">
                <IonMenuButton>
                    <img src={icon} alt=""/>
                </IonMenuButton>
                <h3 className="menu__logo"><strong>Belly Buddy</strong></h3>
            </IonHeader>
        </MenuHeader>
    )
        ;
};

const MenuHeader = styled("div")`
  .menu__header {
    display: flex;
    align-items: center;
    gap: 5px;
    width: 90%;
    margin: 0 auto;
    padding-top: 10px;

    img {
      width: 30px;
    }


    .menu__logo {
      width: calc(100% - 85px);
      display: flex;
      justify-content: center;
      font-size: ${props => props.theme.small};
      font-family: ${props => props.theme.sofiaProSemiBold};
    }

    .menu__icon {
      font-size: ${props => props.theme.extraLarge};
      color: ${props => props.theme.primary};
      cursor: pointer;
    }
  }

  .menu__title {
    font-size: ${props => props.theme.large};
    font-family: ${props => props.theme.sofiaProSemiBold};
  }
`
export default OpenHeader;