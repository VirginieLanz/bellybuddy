import React from "react";
import ReactDOM from "react-dom";
import styled from 'styled-components';
import {Button} from "./Button";

const PremiumModal = ({isShowing, hide, title}) =>
    isShowing
        ? ReactDOM.createPortal(
        <Background>
            <Content className="modal-overlay container--inner">
                <div className="modal__wrapper">
                    <div className="modal">
                        <div className="modal__header">
                            <p>{title}</p>
                        </div>
                        <div className="modal-flex">
                            <p className="modal__text">Cette option n'est pas encore disponible</p>
                            <div className="buttons-container">
                                <Button
                                    className="modal-close-button"
                                    as='button'
                                    type="button"
                                    styleType='button--border-white'
                                    onClick={(evt) => hide()}
                                >
                                    J'ai hâte
                                </Button>
                            </div>
                        </div>
                    </div>
                </div>
            </Content>
        </Background>,
        document.body
        )
        : null;

const Background = styled("div")`
  height: 100vh;
  width: 100vw;
`


const Content = styled("div")`
  color: ${props => props.theme.white};
  border-radius: 20px;
  padding: 20px;
  position: relative;
  background: ${props => props.theme.primary};
  text-align: center;
  top: 50%;
  -ms-transform: translateY(-50%);
  transform: translateY(-50%);


  .modal__header {
    font-size: ${props => props.theme.small};
    font-family: ${props => props.theme.sofiaProSemiBold};
  }

  .modal-flex {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    margin: 0 auto;
  }

  .modal__text {
    margin: 10px 0 30px 0;
  }

  .buttons-container {
    display: flex;
    justify-content: center;
    width: 100%;
  }

`

export default PremiumModal;