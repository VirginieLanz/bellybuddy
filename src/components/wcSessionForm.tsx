import {
    IonContent,
    IonSlides,
    IonSlide,
    IonIcon,
    IonTextarea,
    IonProgressBar
} from '@ionic/react';
import {Storage} from '@capacitor/storage';
import React, {useRef, useState} from "react";
import {useHistory} from "react-router-dom";
import styled from "styled-components";
import {useForm, SubmitHandler} from "react-hook-form";
import type1 from "../assets/type1.svg";
import type2 from "../assets/type2.svg";
import type3 from "../assets/type3.svg";
import type4 from "../assets/type4.svg";
import type5 from "../assets/type5.svg";
import type6 from "../assets/type6.svg";
import type7 from "../assets/type7.svg";
import black from "../assets/black.svg";
import brown from "../assets/brown.svg";
import greenish from "../assets/greenish.svg";
import white from "../assets/white.svg";
import yellow from "../assets/yellow.svg";
import redish from "../assets/redish.svg";
import noSmell from "../assets/no-smell.svg";
import gentleSmell from "../assets/gentle-smell.svg";
import moderateSmell from "../assets/moderate-smell.svg";
import strongSmell from "../assets/strong-smell.svg";
import decreasesSize from "../assets/decreases-size.svg";
import usualSize from "../assets/usual-size.svg";
import superiorSize from "../assets/superior-size.svg";
import littlePain from "../assets/little-pain2.svg";
import moderatePain from "../assets/moderate-pain2.svg";
import excessivePain from "../assets/excessive-pain2.svg";
import lowEffort from "../assets/low-effort.svg";
import moderateEffort from "../assets/moderate-effort.svg";
import excessiveEffort from "../assets/excessive-effort.svg";
import lowFlatulence from "../assets/low-flatulence.svg";
import moderateFlatulence from "../assets/moderate-flatulence.svg";
import excessiveFlatulence from "../assets/excessive-flatulence.svg";
import noSymptoms from "../assets/no-symptoms.svg";
import nausea from "../assets/nausea.svg";
import dizziness from "../assets/dizziness.svg";
import constipation from "../assets/constipation.svg";
import colic from "../assets/colic.svg";
import chills from "../assets/chills.svg";
import urgentNeed from "../assets/urgent-need.svg";
import lessThanThree from "../assets/less-than-three.svg";
import threeToFive from "../assets/three-to-five.svg";
import moreThanFive from "../assets/more-than-five.svg";
import {chevronForwardOutline} from "ionicons/icons";
import {chevronBackOutline} from "ionicons/icons";
import {Button} from "./Button";


//TODO : Régler la progressBAR
const slideOpts = {
    initialSlide: 0,
    speed: 400
};

export enum StoolChartEnum {
    type1 = "Type 1",
    type2 = "Type 2",
    type3 = "Type 3",
    type4 = "Type 4",
    type5 = "Type 5",
    type6 = "Type 6",
    type7 = "Type 7",
}

enum ColorEnum {
    black = "Noir",
    brown = "Marron",
    greenish = "Verdâtre",
    white = "Pâle ou blanc",
    yellow = "Jaune",
    redish = "Rougeâtre"
}

enum SmellEnum {
    scentlessSmell = "Sans odeur",
    gentleSmell = "Douce",
    moderateSmell = "Modérée",
    strongSmell = "Forte"
}

enum SizeEnum {
    decreasesSize = "Diminué",
    usualSize = "Normal",
    superiorSize = "Supérieur"
}

enum PainEnum {
    littlePain = "Pas ou peu de douleurs",
    moderatePain = "Modérément",
    strongPain = "Fortement"
}

enum EffortEnum {
    lowEffort = "Faible",
    moderateEffort = "Modéré",
    excessiveEffort = "Excessif"
}

enum FlatulenceEnum {
    lowFlatulence = "Pas ou peu",
    moderateFlatulence = "Modérée",
    excessiveFlatulence = "Excessive"
}

enum SymptomsEnum {
    noSymptoms = "Pas de symptôme",
    nausea = "Nausées",
    dizziness = "Étourdissements",
    constipation = "Constipation",
    colic = "Colique",
    chills = "Frissons",
    urgentNeed = "Besoin urgent"
}

enum DurationEnum {
    lessThanThree = "< 3 minutes",
    threeToFive = "3 à 5 minutes",
    moreThanFive = "> 5 minutes"
}

interface IFormInput {
    stool: StoolChartEnum;
    color: ColorEnum;
    smell: SmellEnum;
    size: SizeEnum;
    pain: PainEnum;
    effort: EffortEnum;
    flatulence: FlatulenceEnum;
    symptoms: SymptomsEnum;
    duration: DurationEnum;
    notes: string;
}

const wcSessionForm: React.FC = () => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const mySlides = useRef<any>(null);
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [text, setText] = useState<string>();
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const [currentIndex, setCurrentIndex] = useState<number>();
    // eslint-disable-next-line react-hooks/rules-of-hooks

    const setFormData = async (data: any) => {
        // Set default formdata
        const newSession = {
            stool: data.stool,
            color: data.color,
            smell: data.smell,
            size: data.size,
            pain: data.pain,
            effort: data.effort,
            flatulence: data.flatulence,
            symptoms: data.symptoms,
            duration: data.duration,
            notes: data.notes,
            date: new Date(),
            hour: new Date().getHours() + "h" + ('0'+new Date().getMinutes()).slice(-2)
        };

        let sessionArray: any[] = [];

        // Retrieve form data if set
        const sessionObject = await Storage.get({key: 'form'});

        if (sessionObject !== null) {
            // @ts-ignore
            sessionArray = JSON.parse(sessionObject.value);


            if (sessionArray != null) {
                // add new session to the sessionArray
                sessionArray.push(newSession);
            } else {
                // Set new empty sessionArray on the first session
                sessionArray = [
                    newSession
                ];
            }
        }

        // save session array to the storage
        await Storage.set({key: 'form', value: JSON.stringify(sessionArray)});
    }

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const {register, handleSubmit, formState: {errors}} = useForm<IFormInput>();
    const onSubmit: SubmitHandler<IFormInput> = data => {
        setFormData(data);
        history.push('/tabs/homepage');

        // TODO: Find a better fix (exemple : pass the sessionArray data to the other component or smth like that)
        // Used to fix the fact that localstorage has old values and needs a refresh to show the latest ones
        window.location.reload();
    }

    // eslint-disable-next-line react-hooks/rules-of-hooks
    const history = useHistory<string>();

    const prev = async () => {
        await mySlides.current.lockSwipes(false);
        await mySlides.current.slidePrev();
        await mySlides.current.lockSwipes(true);
    }

    const next = async () => {
        await mySlides.current.lockSwipes(false);
        await mySlides.current.slideNext();
        await mySlides.current.lockSwipes(true);
    }

    const handleSlideIndex = (e: any) => {
        console.log(e.target.getActiveIndex());
        e.target.getActiveIndex().then(
            (index: number) => {
                let currentIndex = index + 1;
                setCurrentIndex(currentIndex);
            }
        )
    }

    // Get the length of the slider
    let slidesArray = document.getElementsByClassName("slide__content");
    let slidesArrayLength = slidesArray.length;

    return (
        <Content>
            <form onSubmit={handleSubmit(onSubmit)}>
                <IonSlides pager={true} options={slideOpts} id="slider" ref={mySlides}
                           onIonSlideDidChange={handleSlideIndex} className="container">
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="stool">
                                <p className="slide__title">Quel est le type de votre selle&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="stool-type1" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()} type="radio"
                                               value="Type 1"/>
                                        <img src={type1} alt=""/>
                                        <p>Type 1</p>
                                    </label>
                                    <label htmlFor="stool-type2" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 2"/>
                                        <img src={type2} alt=""/>
                                        <p>Type 2</p>
                                    </label>
                                    <label htmlFor="stool-type3" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 3"/>
                                        <img src={type3} alt=""/>
                                        <p>Type 3</p>
                                    </label>
                                    <label htmlFor="stool-type4" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 4"/>
                                        <img src={type4} alt=""/>
                                        <p>Type 4</p>
                                    </label>
                                    <label htmlFor="stool-type4" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 5"/>
                                        <img src={type5} alt=""/>
                                        <p>Type 5</p>
                                    </label>
                                    <label htmlFor="stool-type6" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 6"/>
                                        <img src={type6} alt=""/>
                                        <p>Type 6</p>
                                    </label>
                                    <label htmlFor="stool-type7" className="slide__option">
                                        <input {...register("stool", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Type 7"/>
                                        <img src={type7} alt=""/>
                                        <p>Type 7</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => history.goBack()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>1/10</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.1}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="color">
                                <p className="slide__title">Quel est sa couleur&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="color-black" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Noir"/>
                                        <img src={black} alt=""/>
                                        <p>Noir</p>
                                    </label>
                                    <label htmlFor="color-brown" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Marron"/>
                                        <img src={brown} alt=""/>
                                        <p>Marron</p>
                                    </label>
                                    <label htmlFor="color-greenish" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Verdâtre"/>
                                        <img src={greenish} alt=""/>
                                        <p>Verdâtre</p>
                                    </label>
                                    <label htmlFor="color-white" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Pâle ou blanc"/>
                                        <img src={white} alt=""/>
                                        <p>Pâle ou blanc</p>
                                    </label>
                                    <label htmlFor="color-yellow" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Jaune"/>
                                        <img src={yellow} alt=""/>
                                        <p>Jaune</p>
                                    </label>
                                    <label htmlFor="color-redish" className="slide__option">
                                        <input {...register("color", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Rougeâtre"/>
                                        <img src={redish} alt=""/>
                                        <p>Rougeâtre</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.2}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="smell">
                                <p className="slide__title">Quel est son odeur&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="smell-scentless" className="slide__option">
                                        <input {...register("smell", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Sans odeur"/>
                                        <img src={noSmell} alt=""/>
                                        <p>Sans odeur</p>
                                    </label>
                                    <label htmlFor="smell-gentle" className="slide__option">
                                        <input {...register("smell", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Douce"/>
                                        <img src={gentleSmell} alt=""/>
                                        <p>Douce</p>
                                    </label>
                                    <label htmlFor="smell-moderate" className="slide__option">
                                        <input {...register("smell", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Modérée"/>
                                        <img src={moderateSmell} alt=""/>
                                        <p>Modérée</p>
                                    </label>
                                    <label htmlFor="smell-strong" className="slide__option">
                                        <input {...register("smell", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Forte"/>
                                        <img src={strongSmell} alt=""/>
                                        <p>Forte</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.3}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="size">
                                <p className="slide__title">Quel est son volume par rapport à d’habitude&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="decreases-size" className="slide__option">
                                        <input {...register("size", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Diminué"/>
                                        <img src={decreasesSize} alt=""/>
                                        <p>Diminué</p>
                                    </label>
                                    <label htmlFor="usual-size" className="slide__option">
                                        <input {...register("size", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Normal"/>
                                        <img src={usualSize} alt=""/>
                                        <p>Normal</p>
                                    </label>
                                    <label htmlFor="superior-size" className="slide__option">
                                        <input {...register("size", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Supérieur"/>
                                        <img src={superiorSize} alt=""/>
                                        <p>Supérieur</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.4}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="pain">
                                <p className="slide__title">Avez-vous eu mal&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="little-pain" className="slide__option">
                                        <input {...register("pain", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Pas ou peu de douleurs"/>
                                        <img src={littlePain} alt=""/>
                                        <p>Pas ou peu de douleurs</p>
                                    </label>
                                    <label htmlFor="moderate-pain" className="slide__option">
                                        <input {...register("pain", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Modérément"/>
                                        <img src={moderatePain} alt=""/>
                                        <p>Modérément</p>
                                    </label>
                                    <label htmlFor="strong-pain" className="slide__option">
                                        <input {...register("pain", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Fortement"/>
                                        <img src={excessivePain} alt=""/>
                                        <p>Excessivement</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.5}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="effort">
                                <p className="slide__title">Quel a été l’effort d’évacutation&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="low-effort" className="slide__option">
                                        <input {...register("effort", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Faible"/>
                                        <img src={lowEffort} alt=""/>
                                        <p>Faible</p>
                                    </label>
                                    <label htmlFor="moderate-effort" className="slide__option">
                                        <input {...register("effort", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Modéré"/>
                                        <img src={moderateEffort} alt=""/>
                                        <p>Modéré</p>
                                    </label>
                                    <label htmlFor="excessive-effort" className="slide__option">
                                        <input {...register("effort", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Excessif"/>
                                        <img src={excessiveEffort} alt=""/>
                                        <p>Excessif</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.6}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="flatulence">
                                <p className="slide__title">Présence de flatulences&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="low-flatulence" className="slide__option">
                                        <input {...register("flatulence", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Pas ou peu"/>
                                        <img src={lowFlatulence} alt=""/>
                                        <p>Faible</p>
                                    </label>
                                    <label htmlFor="moderate-flatulence" className="slide__option">
                                        <input {...register("flatulence", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Modérée"/>
                                        <img src={moderateFlatulence} alt=""/>
                                        <p>Modéré</p>
                                    </label>
                                    <label htmlFor="excessive-flatulence" className="slide__option">
                                        <input {...register("flatulence", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="Excessive"/>
                                        <img src={excessiveFlatulence} alt=""/>
                                        <p>Excessif</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.7}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="symptoms">
                                <p className="slide__title">Quels ont été les symptômes&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="no-symptoms" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Pas de symptôme"
                                               id="noSymptoms"
                                               className="symptoms"
                                               onClick={() => next()}
                                        />
                                        <img src={noSymptoms} alt=""/>
                                        <p>Pas de symptôme</p>
                                    </label>
                                    <label htmlFor="nausea" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Nausées"
                                               className="symptoms"
                                        />
                                        <img src={nausea} alt=""/>
                                        <p>Nausées</p>
                                    </label>
                                    <label htmlFor="dizziness" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Étourdissements"
                                               className="symptoms"
                                        />
                                        <img src={dizziness} alt=""/>
                                        <p>Étourdissements</p>
                                    </label>
                                    <label htmlFor="constipation" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Constipation"
                                               className="symptoms"
                                        />
                                        <img src={constipation} alt=""/>
                                        <p>Constipation</p>
                                    </label>
                                    <label htmlFor="colic" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Colique"
                                               className="symptoms"
                                        />
                                        <img src={colic} alt=""/>
                                        <p>Colique</p>
                                    </label>
                                    <label htmlFor="chills" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Frissons"
                                               className="symptoms"
                                        />
                                        <img src={chills} alt=""/>
                                        <p>Frissons</p>
                                    </label>
                                    <label htmlFor="urgent-need" className="slide__option">
                                        <input {...register("symptoms", {required: true})}
                                               type="checkbox"
                                               value="Besoin urgent"
                                               className="symptoms"
                                        />
                                        <img src={urgentNeed} alt=""/>
                                        <p>Besoin urgent</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav space-between">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                            <button onClick={() => next()} className="button--submit button--small">Suivant <IonIcon
                                icon={chevronForwardOutline}></IonIcon></button>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.8}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="duration">
                                <p className="slide__title">Quelle a été la durée de votre session WC&nbsp;?</p>
                                <div className="slide__options">
                                    <label htmlFor="less-than-three" className="slide__option">
                                        <input {...register("duration", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="< 3 minutes"/>
                                        <img src={lessThanThree} alt=""/>
                                        <p> &lt; 3 minutes</p>
                                    </label>
                                    <label htmlFor="three-to-five" className="slide__option">
                                        <input {...register("duration", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="3 à 5 minutes"/>
                                        <img src={threeToFive} alt=""/>
                                        <p>3 à 5 minutes</p>
                                    </label>
                                    <label htmlFor="more-than-five" className="slide__option">
                                        <input {...register("duration", {required: true})}
                                               onClick={() => next()}
                                               type="radio"
                                               value="> 5 minutes"/>
                                        <img src={moreThanFive} alt=""/>
                                        <p>&gt; 5 minutes</p>
                                    </label>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>
                            <p>{currentIndex}/{slidesArrayLength}</p>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={0.9}></IonProgressBar>
                        </div>
                    </Slide>
                    <Slide>
                        <div className="slide__content">
                            <label htmlFor="notes">
                                <p className="slide__title">Souhaitez-vous ajouter une note concernant cette session
                                    WC&nbsp;?</p>
                                <div className="slide__textarea">
                                    <IonTextarea {...register("notes")}
                                                 placeholder="Ajoutez ici une note que vous souhaitez garder pour cette session WC. "
                                                 value={text}
                                                 onIonChange={(e) => setText(e.detail.value!)}
                                    ></IonTextarea>
                                </div>
                            </label>
                        </div>
                        <div className="slide__nav space-between">
                            <Button
                                as='button'
                                styleType='button--grey'
                                className="button--small"
                                onClick={() => prev()}
                            >
                                <IonIcon icon={chevronBackOutline}></IonIcon>
                                Précédent
                            </Button>

                            <p>{currentIndex}/{slidesArrayLength}</p>
                            <button type="submit" className="button--submit button--small">Terminer</button>
                        </div>
                        <div className="slide__progressBar">
                            <IonProgressBar value={1}></IonProgressBar>
                        </div>
                    </Slide>
                </IonSlides>
            </form>
        </Content>
    )
}

const Content = styled(IonContent)`
  // Radio + checkbox buttons
  [type = radio],
  [type = checkbox] {
    position: absolute;
    opacity: 0;
    width: 80px;
    height: 80px;
    cursor: pointer;
    border-radius: 0;
  }

  [type = radio]:checked + img,
  [type = checkbox]:checked + img,
  [type = radio]:checked + .input-color {
    border-radius: 50px;
    background: ${props => props.theme.gradientPrimary};
  }

  .button--submit {
    border-radius: 12px;
    color: ${props => props.theme.white};
    background: ${props => props.theme.primary};
    height: 100%;
  }

  .button--small {
    font-size: ${props => props.theme.extraSmall};
    padding: 8px 16px;
  }
`

const Slide = styled(IonSlide)`
  display: flex;
  flex-direction: column;
  height: 100vh;

  .slide__title {
    text-align: left;
    color: ${props => props.theme.black};
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    margin-bottom: 70px;
    min-height: 100px;
  }

  .slide__option {
    display: flex;
    align-items: center;
    flex-direction: column;
    width: 30%
  }

  .slide__options {
    width: 90%;
    margin: 0 auto;
    display: flex;
    flex-wrap: wrap;
    gap: 5%;
    row-gap: 50px;

    p {
      font-family: ${props => props.theme.sofiaProLight};
      font-size: ${props => props.theme.extraSmall};
    }

    img {
      width: 60px;
      height: 60px;
      padding: 3px;
      margin-bottom: 5px;
    }

  }

  .slide__content {
    height: 80%;
    width: 100%;
  }

  .slide__textarea {
    textarea {
      border: 1px solid ${props => props.theme.primary};
      padding: 20px;
      border-radius: 20px;
      margin: 0 auto;
      min-height: 300px;
      overflow: scroll;
      text-align: left;
      width: 90%;

      &::placeholder {
        font-size: ${props => props.theme.extraSmall};
      }

    }
  }

  .slide__nav {
    display: flex;
    align-items: center;
    width: 90%;

    &.space-between {
      justify-content: space-between;
    }

    p {
      font-size: ${props => props.theme.small};
      font-family: ${props => props.theme.sofiaProSemiBold};
      width: 33%;
    }
  }
`

export default wcSessionForm;