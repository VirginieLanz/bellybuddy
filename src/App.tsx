import Menu from "./components/Menu";
import Onboarding from "./pages/Onboarding";
import wcSession from "./pages/wcSession";
import wcSessionForm from "./components/wcSessionForm";
import Page from "./pages/Page";
import ArchiveDetails from "./pages/ArchiveDetails";
import React from "react";
import {
    IonApp,
    IonRouterOutlet,
    IonSplitPane,
} from "@ionic/react";
import {IonReactRouter} from "@ionic/react-router";
import {Redirect, Route, Switch} from "react-router-dom";
import styled, {ThemeProvider} from 'styled-components';
import GlobalFonts from './fonts/fonts';

/* Core CSS required for Ionic components to work properly */
import "@ionic/react/css/core.css";

/* Basic CSS for apps built with Ionic */
import "@ionic/react/css/normalize.css";
import "@ionic/react/css/structure.css";
import "@ionic/react/css/typography.css";

/* Optional CSS utils that can be commented out */
import "@ionic/react/css/padding.css";
import "@ionic/react/css/float-elements.css";
import "@ionic/react/css/text-alignment.css";
import "@ionic/react/css/text-transformation.css";
import "@ionic/react/css/flex-utils.css";
import "@ionic/react/css/display.css";

/* Theme variables */
import "./theme/common.css";
import TabRootPage from "./pages/TabRootPage";


const App: React.FC = () => {

    return (
        <IonApp>
            <ThemeProvider theme={theme}>
                <GlobalFonts/>
                <BellyBuddy>
                    <IonReactRouter>
                        <IonSplitPane contentId="main" className={"belly-buddy"}>
                            <Menu/>
                            <IonRouterOutlet id="main">
                                <Switch>
                                    <Route path="/form" component={wcSessionForm} exact/>
                                    <Route path="/onboarding" component={Onboarding} exact/>
                                    <Route path="/wc-session" component={wcSession} exact/>
                                    <Route
                                        path={`/tabs/archives/details`}
                                        render={routeProps => (
                                            <ArchiveDetails data={""} {...routeProps}/>
                                        )}
                                    />
                                    <Route path="/page/:name" component={Page} exact/>
                                    <Route path="/tabs" component={TabRootPage}/>
                                    <Redirect from="/" to="/tabs" exact/>
                                </Switch>
                            </IonRouterOutlet>
                        </IonSplitPane>
                    </IonReactRouter>
                </BellyBuddy>
            </ThemeProvider>
        </IonApp>
    );
};

const BellyBuddy = styled.div`
  line-height: 160%;
`

const theme = {
    primary: '#407BFF',
    gradientPrimary: 'linear-gradient(180deg, rgba(64,123,255,1) 0%, rgba(28,94,242,1) 100%);',
    secondary: '#FFC3BD',
    gradientSecondary: 'linear-gradient(180deg, rgba(255,195,189,1) 0%, rgba(255,165,156,1) 100%);',
    third: '#FFD66C',
    gradientThird: 'linear-gradient(180deg, rgba(255,214,108,1) 0%, rgba(255,196,45,1) 100%);',
    forth: '#79EAC1',
    gradientForth: 'linear-gradient(180deg, rgba(121,234,193,1) 0%, rgba(109,224,183,1) 100%);',
    white: 'white',
    whiteTransparent40: 'rgba(255,255,255,0,4)',
    whiteTransparent80: 'rgba(255,255,255,0,8)',
    darkGrey: '#263238',
    mediumGrey: '#DCDCDC',
    lightGrey: '#F5F5F5',
    minus: '0.6rem',
    extraSmall: '0.8rem',
    small: '1rem',
    medium: '1.2rem',
    large: '1.5rem',
    extraLarge: '2.5rem',
    sofiaProLight: 'Sofia Pro Light',
    sofiaProRegular: 'Sofia Pro Regular',
    sofiaProMedium: 'Sofia Pro Medium',
    sofiaProSemiBold: 'Sofia Pro Semi Bold'
};

export default App;