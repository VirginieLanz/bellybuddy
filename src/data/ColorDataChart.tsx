import {Doughnut} from 'react-chartjs-2'
import {Chart as ChartJS, ArcElement, Tooltip, Legend} from 'chart.js';
import React, {useEffect, useState} from "react";
import {Storage} from "@capacitor/storage";
import styled from "styled-components";
import tips from "../assets/tips-2.svg";
import {arrowForwardOutline} from "ionicons/icons";
import {IonIcon} from "@ionic/react";
import NoSessionWC from "../components/NoSessionWC";
import FirstSessionWC from "../components/FirstSessionWC";

ChartJS.register(ArcElement, Tooltip, Legend);

function ColorDataChart() {
    const [sessionArrayState, setSessionArrayState] = useState<any>([]);
    const [colorState, setColorState] = useState<any>([]);
    const [sessionArray7State, setSessionArray7State] = useState<any>([]);
    const [color, setColor] = useState<string>();
    const [numberOfColor, setNumberOfColor] = useState<any>([]);


    // Tableau des sessions WC
    let sessionArray: any[] = [];
    // Tableau des sessions WC enregistrées durant les 7 derniers jours
    let sessionArray7: any[] = [];
    // Tableau des types de couleurs enregistrées
    let colorArray: any[] = [];

    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});

        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
            setSessionArrayState(sessionArray);
        }
    };

    /**
     * Met à jour le tableau sessionArray7
     */
    const getSessionForTheLast7Days = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let sevenDaysAgoDate = currentDate.getDate() - 7; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de sevenDaysAgoDate
            if (new Date(sessionArray[i].date).getMonth() >= sevenDaysAgoDate) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArray7.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArray7State(sessionArray7);
    }

    const getColorForTheLast7Days = () => {
        for (let i = sessionArray7.length - 1; i >= 0; i--) {
            if (sessionArray7[i].color !== null) {
                colorArray.push(sessionArray7[i].color);
                console.log(sessionArray7[i].color);
            } else {
                break;
            }
        }
        setColorState(colorArray);
        console.log("Color", colorState);
    }

    /**
     * Calcul la moyenne des types d'odeurs des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAverageColor = () => {
        // Variable qui va contenir le nombre de chaque type
        let numberOfColorOccurences = {
            "Noir" : 0,
            "Marron" : 0,
            "Verdâtre" : 0,
            "Pâle ou blanc" : 0,
            "Jaune" : 0,
            "Rougeâtre": 0,
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let session in sessionArray7) {
            // On récupère la couleur et on l'incrémente
            // @ts-ignore
            numberOfColorOccurences[sessionArray7[session].color.toString()] = numberOfColorOccurences[sessionArray7[session].color.toString()] + 1;
        }

        let numberOfColorArray: any = [];
        for (let color in numberOfColorOccurences) {
            // @ts-ignore
            numberOfColorArray.push(numberOfColorOccurences[color]);
        }
        setNumberOfColor(numberOfColorArray);

        // Variable qui set par défaut la couleur la plus fréquente à "Marron"
        let mostFrequentColor: string = "Marron";

        // Récupère la couleur  la plus fréquente
        for (let color in numberOfColorOccurences) {
            // @ts-ignore
            if (numberOfColorOccurences[mostFrequentColor] < numberOfColorOccurences[color]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentColor = color;
            }
        }

        setColor(mostFrequentColor);
    }

    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            getSessionForTheLast7Days();
            getColorForTheLast7Days();
            calculateAverageColor();
        })
    }, []);

    const data = {
        labels: ['Noir', 'Marron', 'Verdâtre', 'Pâle ou blanc', 'Jaune', 'Rougeâtre'],
        datasets: [
            {
                data: numberOfColor,
                backgroundColor: ['rgba(60, 52, 41, 1)', 'rgba(121, 104, 79, 1)', 'rgba(82, 111, 85, 1)', 'rgba(255, 241, 214, 1)', 'rgba(255, 251, 143, 1)', 'rgba(255, 195, 189, 1)'],
            }

        ]
    }

    const options = {
        plugins: {
            title: {
                display: true,
                color: 'blue',
                font: {
                    size: 34
                },
                padding: {
                    top: 30,
                    bottom: 30
                },
                responsive: true,
                animation: {
                    animateScale: true,
                    color: true
                }
            }
        }

    }
    return (
        <Content className="container">
            {colorState.length === 0 ?
                <div className="chart empty flex-content">
                    <NoSessionWC/>
                    <FirstSessionWC/>
                </div>
                :
                <>
                    <div className="chart__titles">
                        <p className="chart__title">Type de couleur la plus fréquente</p>
                        <p className="chart__subtitle">{color}</p>
                    </div>
                    <div className="chart__infos">
                        <p className="chart__info">Analyse effectuée sur {sessionArray7State.length} sessions WC</p>
                    </div>
                    <Doughnut data={data} options={options}/>
                    {color === 'Forte' ?
                        <div className="increase-results">
                            <img src={tips} alt=""/>
                            <p>Améliorer ces résultats <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                        </div>
                        :
                        <></>
                    }
                </>
            }
        </Content>
    )
}

const Content = styled("div")`
  margin-bottom: 160px;
  p {
    text-align: center;
  }

  .chart__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.small};
  }

  .chart__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    color: ${props => props.theme.primary};
    margin: 5px 0 30px 0;
  }

  .chart__info {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-bottom: 20px;
  }

  .increase-results {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 80%;
    background: ${props => props.theme.gradientSecondary};
    color: ${props => props.theme.white};
    font-family: ${props => props.theme.sofiaProSemiBold};
    margin: -20px auto 70px auto;
    border-radius: 20px;
    padding: 15px;
    height: min-content;

    p {
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      gap: 10px;
    }
  }

  .chart {
    &.empty {
      margin-top: 20px;
    }
  }
`


export default ColorDataChart