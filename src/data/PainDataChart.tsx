import {Doughnut} from 'react-chartjs-2'
import {Chart as ChartJS, ArcElement, Tooltip, Legend} from 'chart.js';
import React, {useEffect, useState} from "react";
import {Storage} from "@capacitor/storage";
import styled from "styled-components";
import NoSessionWC from "../components/NoSessionWC";
import FirstSessionWC from "../components/FirstSessionWC";
import tips from "../assets/tips-2.svg";
import {IonIcon} from "@ionic/react";
import {arrowForwardOutline} from "ionicons/icons";

ChartJS.register(ArcElement, Tooltip, Legend);

function PainDataChart() {
    const [sessionArrayState, setSessionArrayState] = useState<any>([]);
    const [painState, setPainState] = useState<any>([]);
    const [sessionArray7State, setSessionArray7State] = useState<any>([]);
    const [pain, setPain] = useState<string>();
    const [numberOfPain, setNumberOfPain] = useState<any>([]);


    // Tableau des sessions WC
    let sessionArray: any[] = [];
    // Tableau des sessions WC enregistrées durant les 7 derniers jours
    let sessionArray7: any[] = [];
    // Tableau des types de douleurs enregistrés
    let painArray: any[] = [];

    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});

        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
            setSessionArrayState(sessionArray);
        }
    };

    /**
     * Met à jour le tableau sessionArray7
     */
    const getSessionForTheLast7Days = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let sevenDaysAgoDate = currentDate.getDate() - 7; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de sevenDaysAgoDate
            if (new Date(sessionArray[i].date).getMonth() >= sevenDaysAgoDate) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArray7.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArray7State(sessionArray7);
    }

    const getPainForTheLast7Days = () => {
        for (let i = sessionArray7.length - 1; i >= 0; i--) {
            if (sessionArray7[i].pain !== null) {
                painArray.push(sessionArray7[i].pain);
                console.log(sessionArray7[i].pain);
            } else {
                break;
            }
        }
        setPainState(painArray);
        console.log("Pain", painState);
    }

    /**
     * Calcul la moyenne des différentes duoleurs des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAveragePain = () => {
        // Variable qui va contenir le nombre de chaque douleur
        let numberOfPainOccurences = {
            "Pas ou peu de douleurs" : 0,
            "Modérément" : 0,
            "Fortement" : 0,
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let pain in sessionArray7) {
            // On récupère la douleur et on l'incrémente
            // @ts-ignore
            numberOfPainOccurences[sessionArray7[pain].pain.toString()] = numberOfPainOccurences[sessionArray7[pain].pain.toString()] + 1;
        }

        let numberOfPainArray: any = [];
        for (let pain in numberOfPainOccurences) {
            // @ts-ignore
            numberOfPainArray.push(numberOfPainOccurences[pain]);
        }
        setNumberOfPain(numberOfPainArray);

        let mostFrequentPain: string = "Pas ou peu de douleurs";

        // Récupère la douleur  la plus fréquente
        for (let pain in numberOfPainOccurences) {
            // @ts-ignore
            if (numberOfPainOccurences[mostFrequentPain] < numberOfPainOccurences[pain]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentPain = pain;
            }
        }

        setPain(mostFrequentPain);
    }

    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            getSessionForTheLast7Days();
            getPainForTheLast7Days();
            calculateAveragePain();
        })
    }, []);

    const data = {
        labels: ['Pas ou peu de douleurs', 'Modérément', 'Fortement'],
        datasets: [
            {
                data: numberOfPain,
                backgroundColor: ['rgba(121, 234, 193, 1)', 'rgba(255, 216, 79, 1)', 'rgba(255, 195, 189, 1)'],
            }

        ]
    }

    const options = {
        plugins: {
            title: {
                display: true,
                color: 'blue',
                font: {
                    size: 34
                },
                padding: {
                    top: 30,
                    bottom: 30
                },
                responsive: true,
                animation: {
                    animateScale: true,
                    color: true
                }
            }
        }

    }
    return (
        <Content className="container">
            {painState.length === 0 ?
                <div className="chart empty flex-content">
                    <NoSessionWC/>
                    <FirstSessionWC/>
                </div>
                :
                <>
                    <div className="chart__titles">
                        <p className="chart__title">Type de douleur la plus fréquente</p>
                        <p className="chart__subtitle">{pain}</p>
                    </div>
                    <div className="chart__infos">
                        <p className="chart__info">Analyse effectuée sur {sessionArray7State.length} sessions WC</p>
                    </div>
                    <Doughnut data={data} options={options}/>
                    {pain === 'Fortement' ?
                        <div className="increase-results">
                            <img src={tips} alt=""/>
                            <p>Améliorer ces résultats <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                        </div>
                        :
                        <></>
                    }
                </>
            }
        </Content>
    )
}

const Content = styled("div")`
  margin-bottom: 160px;
  p {
    text-align: center;
  }

  .chart__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.small};
  }

  .chart__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    color: ${props => props.theme.primary};
    margin: 5px 0 30px 0;
  }

  .chart__info {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-bottom: 20px;
  }

  .increase-results {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 80%;
    background: ${props => props.theme.gradientSecondary};
    color: ${props => props.theme.white};
    font-family: ${props => props.theme.sofiaProSemiBold};
    margin: -20px auto 70px auto;
    border-radius: 20px;
    padding: 15px;
    height: min-content;

    p {
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      gap: 10px;
    }
  }

  .chart {
    &.empty {
      margin-top: 20px;
    }
  }
`


export default PainDataChart