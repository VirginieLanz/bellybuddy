import {Doughnut} from 'react-chartjs-2'
import {Chart as ChartJS, ArcElement, Tooltip, Legend} from 'chart.js';
import React, {useEffect, useState} from "react";
import {Storage} from "@capacitor/storage";
import styled from "styled-components";
import tips from "../assets/tips-2.svg";
import {arrowForwardOutline} from "ionicons/icons";
import {IonIcon} from "@ionic/react";
import NoSessionWC from "../components/NoSessionWC";
import FirstSessionWC from "../components/FirstSessionWC";
import {Link} from 'react-router-dom';

ChartJS.register(ArcElement, Tooltip, Legend);

function StoolTypeDataChart() {
    const [sessionArrayState, setSessionArrayState] = useState<any>([]);
    const [stoolTypeState, setStoolTypeState] = useState<any>([]);
    const [sessionArray7State, setSessionArray7State] = useState<any>([]);
    const [stoolType, setStoolType] = useState<string>();
    const [numberOfStoolType, setNumberOfStoolType] = useState<any>([]);


    // Tableau des sessions WC
    let sessionArray: any[] = [];
    // Tableau des sessions WC enregistrées durant les 7 derniers jours
    let sessionArray7: any[] = [];
    // Tableau des types de selles enregistrées
    let stoolTypeArray: any[] = [];

    /**
     * Récupération des sessions WC
     */
    const getFormData = async () => {
        // Fonction de Capacitor Storage pour récupérer les données du formulaire
        const sessionObject = await Storage.get({key: 'form'});

        if (typeof sessionObject.value === "string") {
            sessionArray = JSON.parse(sessionObject.value);
            setSessionArrayState(sessionArray);
        }
    };

    /**
     * Met à jour le tableau sessionArray7
     */
    const getSessionForTheLast7Days = () => {
        // Récupération de la date courante
        let currentDate = new Date();
        let sevenDaysAgoDate = currentDate.getDate() - 7; // Int

        // Le tableau sessionArray est parsé en partant de la fin (depuis la plus récente session enregistrée)
        for (let i = sessionArray.length - 1; i >= 0; i--) {
            // On vérifie si la valeur date du tableau (typée en int grâce à new Date()) est plus grande ou égale à la valeur de sevenDaysAgoDate
            if (new Date(sessionArray[i].date).getMonth() >= sevenDaysAgoDate) {
                // Si oui, on a ajoute la session dans le tableau sessionArray7
                sessionArray7.push(sessionArray[i]);
            } else {
                break;
            }
        }
        setSessionArray7State(sessionArray7);
    }

    const getStoolTypeForTheLast7Days = () => {
        for (let i = sessionArray7.length - 1; i >= 0; i--) {
            if (sessionArray7[i].stool !== null) {
                stoolTypeArray.push(sessionArray7[i].stool);
                console.log(sessionArray7[i].stool);
            } else {
                break;
            }
        }
        setStoolTypeState(stoolTypeArray);
        console.log("Stool", stoolTypeState);
    }

    /**
     * Calcul la moyenne des types de selles des sessions enregistrées durant les 7 derniers jours
     */
    const calculateAverageType = () => {
        // Variable qui va contenir le nombre de chaque type
        let numberOfTypeOccurences = {
            "Type 1": 0,
            "Type 2": 0,
            "Type 3": 0,
            "Type 4": 0,
            "Type 5": 0,
            "Type 6": 0,
            "Type 7": 0
        }

        // Pour chaque sessions enregistrées ces 7 derniers jours
        for (let session in sessionArray7) {
            // On récupère le type et on l'incrémente
            // @ts-ignore
            numberOfTypeOccurences[sessionArray7[session].stool.toString()] = numberOfTypeOccurences[sessionArray7[session].stool.toString()] + 1;
        }

        let numberOfStoolTypeArray: any = [];
        for (let type in numberOfTypeOccurences) {
            // @ts-ignore
            numberOfStoolTypeArray.push(numberOfTypeOccurences[type]);
        }
        setNumberOfStoolType(numberOfStoolTypeArray);

        // Variable qui set par défaut la durée la plus fréquente à "< 3"
        let mostFrequentStool: string = "Type 1";

        // Récupère la durée la plus fréquente
        for (let type in numberOfTypeOccurences) {
            // @ts-ignore
            if (numberOfTypeOccurences[mostFrequentStool] < numberOfTypeOccurences[type]) {
                // Mise à jour de la variable si jamais la clé qui est en train d'être checkée a une valeur plus grande que la précédente valeur
                mostFrequentStool = type;
            }
        }

        setStoolType(mostFrequentStool);
    }

    useEffect(() => {
        // Dès que la homepage est montée, on récupère les données du formulaire
        getFormData().then(() => {
            getSessionForTheLast7Days();
            getStoolTypeForTheLast7Days();
            calculateAverageType();
        })
    }, []);

    const data = {
        labels: ['Type 1', 'Type 2', 'Type 3', 'Type 4', 'Type 5', 'Type 6', 'Type 7'],
        datasets: [
            {
                data: numberOfStoolType,
                backgroundColor: ['rgba(255, 195, 189, 1)', 'rgba(255, 216, 79, 1)', 'rgba(121, 234, 193, 1)', 'rgba(121, 234, 193, 1)', 'rgba(255, 216, 79, 1)', 'rgba(255, 216, 79, 1)', 'rgba(255, 195, 189, 1)'],
            }

        ]
    }

    const options = {
        plugins: {
            title: {
                display: true,
                color: 'blue',
                font: {
                    size: 34
                },
                padding: {
                    top: 30,
                    bottom: 30
                },
                responsive: true,
                animation: {
                    animateScale: true,
                    color: true
                }
            }
        }

    }
    return (
        <Content className="container">
            {stoolTypeState.length === 0 ?
                <div className="chart empty flex-content">
                    <NoSessionWC/>
                    <FirstSessionWC/>
                </div>
                :
                <>
                    <div className="chart__titles">
                        <p className="chart__title">Type de selles le plus fréquent</p>
                        <p className="chart__subtitle">{stoolType}</p>
                    </div>
                    <div className="chart__infos">
                        <p className="chart__info">Analyse effectuée sur {sessionArray7State.length} sessions WC</p>
                    </div>
                    <Doughnut data={data} options={options}/>
                    {stoolType === 'Type 1' || stoolType === 'Type 7'  ?
                        <Link to="/tabs/advices">
                            <div className="increase-results">
                                <img src={tips} alt=""/>
                                <p>Améliorer ces résultats <IonIcon icon={arrowForwardOutline}></IonIcon></p>
                            </div>
                        </Link>
                        :
                        <></>
                    }
                </>
            }
        </Content>
    )
}

const Content = styled("div")`
  margin-bottom: 160px;
  p {
    text-align: center;
  }
  
  a {
    text-decoration: none;
  }

  .chart__title {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.small};
  }

  .chart__subtitle {
    font-family: ${props => props.theme.sofiaProSemiBold};
    font-size: ${props => props.theme.large};
    color: ${props => props.theme.primary};
    margin: 5px 0 30px 0;
  }
  
  .chart__info {
    font-family: ${props => props.theme.sofiaProLight};
    font-size: ${props => props.theme.extraSmall};
    margin-bottom: 20px;
  }
  
  .increase-results {
    display: flex;
    flex-direction: row;
    align-items: center;
    width: 80%;
    background: ${props => props.theme.gradientSecondary};
    color: ${props => props.theme.white};
    font-family: ${props => props.theme.sofiaProSemiBold};
    margin: -20px auto 70px auto;
    border-radius: 20px;
    padding: 15px;
    height: min-content;
    
    p {
      width: 100%;
      display: flex;
      align-items: center;
      justify-content: center;
      gap: 10px;
    }
  }
  
  .chart {
    &.empty {
      margin-top: 20px;
    }
  }
`


export default StoolTypeDataChart