import { createGlobalStyle } from 'styled-components';
import SofiaProLight from './SofiaProLight.woff';
import SofiaProRegular from './SofiaProRegular.woff';
import SofiaProMedium from './SofiaProMedium.woff2';
import SofiaProSemiBold from './SofiaProSemiBold.woff2';

export default createGlobalStyle`
    @font-face {
      @font-face {
        font-family: 'Sofia Pro Light';
        src: local('Sofia Pro Light'), local('SofiaProLight'),
        url(${SofiaProLight}) format('woff2');
      }
      
      font-family: 'Sofia Pro Regular';
      src: local('Sofia Pro Regular'), local('SofiaProRegular'),
      url(${SofiaProRegular}) format('woff');
      font-weight: 300;
      font-style: normal;
    }

    @font-face {
      font-family: 'Sofia Pro Medium';
      src: local('Sofia Pro Medium'), local('SofiaProMedium'),
      url(${SofiaProMedium}) format('woff2');
    }

    @font-face {
      font-family: 'Sofia Pro SemiBold';
      src: local('Sofia Pro SemiBold'), local('SofiaProSemiBold'),
      url(${SofiaProSemiBold}) format('woff2');
    }
`;
